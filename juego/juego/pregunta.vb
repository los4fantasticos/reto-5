﻿Imports Oracle.ManagedDataAccess.Client
Public Class pregunta
    Public Const PUNTUACIONRESPUESTA = 1000
    Public Const PREGUNTA = 120
    Public Const RESPUESTA = 300
    Public Const EXPLICACION = 420
    Public Const PUNTUACIONBONUS = 5000
    Public Const CONTADORBONUS = 3

    Dim nivel As Integer = 9
    'Dim nivel As Integer = 2

    Dim dsPartidas As DataSet
    Dim dsPreguntas As DataSet
    Dim daPartidas As OracleDataAdapter
    Dim daPreguntas As OracleDataAdapter
    Dim cmBuild, cmBuild2 As OracleCommandBuilder

    Dim contador As Integer
    Dim contadorPregunta As Integer
    Dim imagen As Object
    Public puntuacion As Integer
    Dim respuestas As Object

    Dim arrayNumeros As ArrayList
    Dim arrayPuntuaciones As ArrayList
    Dim arrayTiempos As ArrayList
    Dim arrayHaContestado As ArrayList

    Dim bonus As Integer

    Dim numPregunta As Integer
    Dim totalPreguntas As Integer


    Private Sub pregunta_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.Location = MenuJuego.Location
        arrayPuntuaciones = New ArrayList
        arrayTiempos = New ArrayList
        arrayHaContestado = New ArrayList
        contador = 0
        contadorPregunta = 0
        puntuacion = 0
        bonus = 0
        lblTiempo.Text = PREGUNTA / 10



        If (TipoJuego.tipo = "ingles") Then
            lblMemoriza.Text = "MEMORIZE THIS IMAGE"
            Label2.Text = "Time:"
            Label1.Text = "Score:"
        Else
            lblMemoriza.Text = "MEMORIZA ESTA IMAGEN"
            Label2.Text = "Tiempo:"
            Label1.Text = "Puntuación:"
        End If

        numPregunta = 0
        totalPreguntas = MenuJuego.preguntas.count


        reloj.Start()

    End Sub

    Private Sub reloj_Tick(sender As Object, e As EventArgs) Handles reloj.Tick


        '-----------OPACIDAD-------
        'If contador Mod 20 = 0 Then

        '    Me.Opacity = Me.Opacity - 0.2
        'End If

        sacarImagen()
        sacarPregunta()
        sacarExplicacion()
        controlarBotonOmitir()
        controlarPausa()


        If contador = EXPLICACION Then
            siguientePregunta()
            lblTiempo.Text = (PREGUNTA - contador) / 10
        End If

        If contadorPregunta = nivel + 1 Then
            reloj.Stop()
        End If

        If contador = RESPUESTA - 2 Then
            arrayTiempos.Add((RESPUESTA - PREGUNTA) / 10)
            arrayPuntuaciones.Add(0)
            arrayHaContestado.Add(0)
            bonus = 0
            My.Computer.Audio.Play(My.Resources.perder, AudioPlayMode.Background)
            If (TipoJuego.tipo = "ingles") Then
                lblCorrecto.Text = "UNANSWERED"
            Else
                lblCorrecto.Text = "SIN CONTESTAR"
            End If


            PBBienMal.Image = My.Resources._error

            comprobarBonus()

        End If

        If contador = RESPUESTA + 40 Then
            lblBonusPuntuacion.Hide()
        End If

    End Sub

    Private Sub comprobarBonus()
        Select Case bonus
            Case 0
                PictureBox1.Show()
                PictureBox2.Show()
                PictureBox3.Show()
            Case 1
                PictureBox3.Hide()
            Case 2

                PictureBox2.Hide()
            Case 3
                PictureBox3.Hide()

        End Select

    End Sub

    Private Sub controlarPausa()
        If contador > RESPUESTA And contador < EXPLICACION Then
            lblPausar.Visible = True
        Else
            lblPausar.Visible = False
        End If
    End Sub

    Private Sub controlarBotonOmitir()
        If contador = PREGUNTA / 2 Then
            lblOmitir.Visible = True
        ElseIf contador = PREGUNTA Then
            lblOmitir.Visible = False
        ElseIf contador = RESPUESTA + (EXPLICACION - RESPUESTA) / 2 Then
            lblOmitir.Visible = True
        ElseIf contador = 0 Then
            lblOmitir.Visible = False

        End If



    End Sub

    Private Sub sacarExplicacion()

        If contador = RESPUESTA Then
            mostrarExplicacion(contadorPregunta)

        End If

        If contador >= RESPUESTA And contador < EXPLICACION Then
            If contador Mod 10 = 0 Then
                lblTiempo.Text = (EXPLICACION - contador) / 10
            End If

        End If
    End Sub

    Private Sub sacarPregunta()


        If contador = PREGUNTA Then
            lblMemoriza.Hide()


            Me.BackgroundImage = My.Resources.fondoPreguntasOk

            PBImagen.Hide()

            cargarPregunta(contadorPregunta)
            mostrarPregunta(True)
        End If

        If contador >= PREGUNTA And contador < RESPUESTA Then
            If contador Mod 10 = 0 Then
                lblTiempo.Text = (RESPUESTA - contador) / 10
            End If

        End If

        contador = contador + 1
    End Sub

    Private Sub sacarImagen()
        If contador = 0 Then
            Try
                imagen = MenuJuego.preguntas(contadorPregunta)("imagen")
                PBImagen.ImageLocation = "http://" & imagen("ruta")
            Catch ex As Exception
                'MsgBox("Imagen no encontrada")
            End Try
            numPregunta = numPregunta + 1
            lblNumeroPregunta.Text = "PREGUNTA " & numPregunta & " - " & totalPreguntas


            PBImagen.Show()
            lblMemoriza.Show()

            PBImagen.Size = New Size(contador, contador)
            lblCorrecto.Visible = False
            PBBienMal.Visible = False

        End If

        If PBImagen.Width < 720 Then
            PBImagen.Size = New Size(contador * 24, contador * 16)


        End If

        If contador > 0 And contador < PREGUNTA Then
            If contador Mod 10 = 0 Then
                lblTiempo.Text = (PREGUNTA - contador) / 10
            End If

        End If
    End Sub

    Private Sub mostrarPregunta(v As Boolean)
        lblPregunta.Visible = v
        LblMarcoPregunta.Visible = v
        lblRespuesta1.Visible = v
        lblRespuesta2.Visible = v
        lblRespuesta3.Visible = v
        lblRespuesta4.Visible = v


    End Sub

    Private Sub cargarPregunta(contadorPregunta As Integer)
        respuestas = MenuJuego.preguntas(contadorPregunta)("respuestas")
        lblPregunta.Text = MenuJuego.preguntas(contadorPregunta)("pregunta")
        Randomize()
        Dim aleatorio As Integer
        arrayNumeros = New ArrayList

        Do While arrayNumeros.Count < 4

            aleatorio = Int((4 * Rnd()) + 1) - 1
            If arrayNumeros.Count = 0 Then
                arrayNumeros.Add(aleatorio)
            Else
                If Not arrayNumeros.Contains(aleatorio) Then
                    arrayNumeros.Add(aleatorio)

                End If

            End If
        Loop


        lblRespuesta1.Text = respuestas(arrayNumeros(0))("respuesta")
        lblRespuesta2.Text = respuestas(arrayNumeros(1))("respuesta")
        lblRespuesta3.Text = respuestas(arrayNumeros(2))("respuesta")
        lblRespuesta4.Text = respuestas(arrayNumeros(3))("respuesta")
        My.Computer.Audio.Play(My.Resources.un_dos_tres_reloj_cuco, AudioPlayMode.BackgroundLoop)

    End Sub

    Private Sub lblRespuesta1_Click(sender As Object, e As EventArgs) Handles lblRespuesta1.Click
        comprobarRespuesta(respuestas(arrayNumeros(0))("verdadero"))
        contador = RESPUESTA - 1
    End Sub



    Private Sub lblRespuesta2_Click(sender As Object, e As EventArgs) Handles lblRespuesta2.Click
        comprobarRespuesta(respuestas(arrayNumeros(1))("verdadero"))
        contador = RESPUESTA - 1
    End Sub



    Private Sub lblRespuesta3_Click(sender As Object, e As EventArgs) Handles lblRespuesta3.Click
        comprobarRespuesta(respuestas(arrayNumeros(2))("verdadero"))
        contador = RESPUESTA - 1
    End Sub

    Private Sub lblRespuesta4_Click(sender As Object, e As EventArgs) Handles lblRespuesta4.Click
        comprobarRespuesta(respuestas(arrayNumeros(3))("verdadero"))
        contador = RESPUESTA - 1
    End Sub

    Private Sub siguientePregunta()
        My.Computer.Audio.Stop()
        PBExplicacion.Hide()
        If (contadorPregunta < nivel) Then
            reloj.Start()
            contadorPregunta = contadorPregunta + 1
            contador = 0
            mostrarPregunta(False)
            lblRespuestaCorrecta.Hide()
            LblSeparar.Hide()
            LblRC.Hide()
            lblExplicacion.Hide()
            lblTiempo.Text = (PREGUNTA - contador) / 10
            lblOmitir.Visible = False

        Else
            reloj.Stop()
            guardarPartida()

            Me.Hide()
            pantallaFinal.Show()
            'MsgBox("juego terminado")
        End If

    End Sub



    Private Sub comprobarRespuesta(p As Object)
        Dim puntuacionTiempo As Integer
        puntuacionTiempo = (RESPUESTA - contador) * 10


        If p = 1 Then
            bonus = bonus + 1
            If bonus = CONTADORBONUS Then
                puntuacion = puntuacion + PUNTUACIONBONUS
                lblBonusPuntuacion.Show()
                arrayPuntuaciones.Add(PUNTUACIONRESPUESTA + puntuacionTiempo + PUNTUACIONBONUS)
                bonus = 0
            Else
                arrayPuntuaciones.Add(PUNTUACIONRESPUESTA + puntuacionTiempo)

            End If

            My.Computer.Audio.Play(My.Resources.acierto, AudioPlayMode.Background)

            If (TipoJuego.tipo = "ingles") Then
                lblCorrecto.Text = "CORRECT ANSWER"
            Else
                lblCorrecto.Text = "RESPUESTA CORRECTA"
            End If

            lblCorrecto.ForeColor = Color.LightGreen
            PBBienMal.Image = My.Resources.correcto
            puntuacion = puntuacion + PUNTUACIONRESPUESTA + puntuacionTiempo

            arrayTiempos.Add((contador - PREGUNTA) / 10)
            arrayHaContestado.Add(1)
        Else
            bonus = 0
            My.Computer.Audio.Play(My.Resources.perder, AudioPlayMode.Background)
            If (TipoJuego.tipo = "ingles") Then
                lblCorrecto.Text = "WRONG ANSWER"
            Else
                lblCorrecto.Text = "RESPUESTA INCORRECTA"
            End If

            lblCorrecto.ForeColor = Color.Red
            PBBienMal.Image = My.Resources._error
            arrayPuntuaciones.Add(0)
            arrayTiempos.Add((contador - PREGUNTA) / 10)
            arrayHaContestado.Add(1)

        End If

        lblCorrecto.Visible = True
        PBBienMal.Visible = True

        comprobarBonus()

        lblPuntuacion.Text = puntuacion

    End Sub

    Private Sub mostrarExplicacion(contadorPregunta As Integer)
        mostrarPregunta(False)

        Me.BackgroundImage = My.Resources.fondoFotoOk


        For Each r In respuestas
            If r("verdadero") = 1 Then
                lblRespuestaCorrecta.Text = r("respuesta")
                Exit For
            End If
        Next
        lblExplicacion.Text = MenuJuego.preguntas(contadorPregunta)("explicacion")

        If (TipoJuego.tipo = "ingles") Then
            LblRC.Text = "Correct Answer:"
        Else
            LblRC.Text = "Respuesta Correcta:"
        End If


        PBExplicacion.Image = PBImagen.Image

        PBExplicacion.Show()
        LblRC.Show()
        LblSeparar.Show()
        lblRespuestaCorrecta.Show()
        lblExplicacion.Show()
        lblCorrecto.Show()
        PBBienMal.Visible = True

    End Sub

    Private Sub lblOmitir_Click(sender As Object, e As EventArgs) Handles lblOmitir.Click
        If contador < PREGUNTA Then
            contador = PREGUNTA
            lblOmitir.Visible = False
        Else
            lblOmitir.Visible = False
            lblPausar.Visible = False
            If lblPausar.Text = "CONTINUAR" Then
                lblPausar.Text = "PAUSAR"
                lblPausar.Image = My.Resources.pausar2

            End If

            siguientePregunta()

        End If

    End Sub

    Private Sub lblPausar_Click(sender As Object, e As EventArgs) Handles lblPausar.Click
        Dim img1, img2 As Bitmap
        img1 = My.Resources.continuar
        img2 = My.Resources.pausar2




        If lblPausar.Text.ToLower = "pausar" Then
            reloj.Stop()
            lblPausar.Text = "CONTINUAR"
            lblPausar.Image = img1
        Else
            reloj.Start()
            lblPausar.Text = "PAUSAR"
            lblPausar.Image = img2
        End If
    End Sub
    Private Sub guardarPartida()

        dsPartidas = New DataSet

        Try

            daPartidas = New OracleDataAdapter("select * from partida order by id_partida", Form1.oracleConnection1)

            cmBuild = New OracleCommandBuilder(daPartidas)

            daPartidas.Fill(dsPartidas, "partida")

        Catch ex As Exception
            MsgBox("Error al conectar con la base de datos , inténtelo mas tarde por favor")
            Return
        End Try


        dsPartidas.Tables("partida").Columns("id_partida").AutoIncrement = True

        If dsPartidas.Tables("partida").Rows.Count > 0 Then
            dsPartidas.Tables("partida").Columns("id_partida").AutoIncrementSeed = dsPartidas.Tables("partida").Rows(dsPartidas.Tables("partida").Rows.Count - 1).Item("id_partida") + 1
        Else
            dsPartidas.Tables("partida").Columns("id_partida").AutoIncrementSeed = 1
        End If


        dsPartidas.Tables("partida").Columns("id_partida").AutoIncrement = 1

        Dim dr As DataRow
        dr = dsPartidas.Tables("partida").NewRow
        dr.Item("fecha") = DateTime.Now
        dr.Item("id_tipo_juego") = TipoJuego.id_tipo_juego
        dr.Item("id_usuario") = Form1.idUsuario

        dsPartidas.Tables("partida").Rows.Add(dr)
        Try
            If daPartidas.Update(dsPartidas, "partida") = 0 Then
                MsgBox("Ha habido un error al guardar datos. Intentalo mas tarde")
            Else

                guardarPreguntas()

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub lblSalir_Click(sender As Object, e As EventArgs) Handles lblSalir.Click
        My.Computer.Audio.Stop()
        End
    End Sub

    Private Sub guardarPreguntas()
        dsPreguntas = New DataSet

        Try

            daPreguntas = New OracleDataAdapter("select * from pregunpar order by id_pregunpar", Form1.oracleConnection1)


            cmBuild2 = New OracleCommandBuilder(daPreguntas)

            daPreguntas.Fill(dsPreguntas, "pregunpar")

        Catch ex As Exception
            MsgBox("Error al conectar con la base de datos , inténtelo mas tarde por favor")
            Return
        End Try

        dsPreguntas.Tables("pregunpar").Columns("id_pregunpar").AutoIncrement = True

        If dsPreguntas.Tables("pregunpar").Rows.Count > 0 Then
            dsPreguntas.Tables("pregunpar").Columns("id_pregunpar").AutoIncrementSeed = dsPreguntas.Tables("pregunpar").Rows(dsPreguntas.Tables("pregunpar").Rows.Count - 1).Item("id_pregunpar") + 1
        Else
            dsPreguntas.Tables("pregunpar").Columns("id_pregunpar").AutoIncrementSeed = 1
        End If

        dsPreguntas.Tables("pregunpar").Columns("id_pregunpar").AutoIncrement = 1

        Dim dr As DataRow

        For i As Integer = 0 To arrayPuntuaciones.Count - 1
            dr = dsPreguntas.Tables("pregunpar").NewRow
            dr.Item("id_partida") = dsPartidas.Tables("partida").Rows(dsPartidas.Tables("partida").Rows.Count - 1).Item("id_partida")

            dr.Item("id_mongo_pregunta") = MenuJuego.preguntas(i)("_id")
            dr.Item("tiempo_respuesta") = arrayTiempos(i)
            dr.Item("score") = arrayPuntuaciones(i)
            dr.Item("ha_respondido") = arrayHaContestado(i)
            dsPreguntas.Tables("pregunPar").Rows.Add(dr)

        Next




        Try
            If daPreguntas.Update(dsPreguntas, "pregunpar") = 0 Then
                MsgBox("Ha habido un error al guardar datos. Intentalo mas tarde")
            Else

                ' MsgBox("Preguntas guardadas con exito")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
End Class