﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TipoJuego
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TipoJuego))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PBoxFol = New System.Windows.Forms.PictureBox()
        Me.PBoxEnglish = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.lblSalir = New System.Windows.Forms.Label()
        CType(Me.PBoxFol, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBoxEnglish, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(373, 278)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(391, 80)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "SELECCIONA UN OPCIÓN  SELECT AN OPTION"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PBoxFol
        '
        Me.PBoxFol.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PBoxFol.Image = Global.juego.My.Resources.Resources.prl
        Me.PBoxFol.Location = New System.Drawing.Point(659, 384)
        Me.PBoxFol.Name = "PBoxFol"
        Me.PBoxFol.Size = New System.Drawing.Size(391, 214)
        Me.PBoxFol.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBoxFol.TabIndex = 10
        Me.PBoxFol.TabStop = False
        '
        'PBoxEnglish
        '
        Me.PBoxEnglish.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PBoxEnglish.Image = Global.juego.My.Resources.Resources.INGLES
        Me.PBoxEnglish.Location = New System.Drawing.Point(116, 384)
        Me.PBoxEnglish.Name = "PBoxEnglish"
        Me.PBoxEnglish.Size = New System.Drawing.Size(391, 214)
        Me.PBoxEnglish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBoxEnglish.TabIndex = 9
        Me.PBoxEnglish.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.juego.My.Resources.Resources.LogoReto5
        Me.PictureBox1.Location = New System.Drawing.Point(111, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(986, 283)
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Showcard Gothic", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Image = CType(resources.GetObject("Label2.Image"), System.Drawing.Image)
        Me.Label2.Location = New System.Drawing.Point(819, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 26)
        Me.Label2.TabIndex = 12
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.BackColor = System.Drawing.Color.Transparent
        Me.lblUsuario.Font = New System.Drawing.Font("Carton Six", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsuario.ForeColor = System.Drawing.Color.Azure
        Me.lblUsuario.Location = New System.Drawing.Point(934, 21)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(72, 27)
        Me.lblUsuario.TabIndex = 13
        Me.lblUsuario.Text = "Label3"
        '
        'lblSalir
        '
        Me.lblSalir.BackColor = System.Drawing.Color.Transparent
        Me.lblSalir.Image = CType(resources.GetObject("lblSalir.Image"), System.Drawing.Image)
        Me.lblSalir.Location = New System.Drawing.Point(30, 25)
        Me.lblSalir.Name = "lblSalir"
        Me.lblSalir.Size = New System.Drawing.Size(30, 30)
        Me.lblSalir.TabIndex = 14
        '
        'TipoJuego
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1099, 620)
        Me.Controls.Add(Me.lblSalir)
        Me.Controls.Add(Me.lblUsuario)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PBoxFol)
        Me.Controls.Add(Me.PBoxEnglish)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Showcard Gothic", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "TipoJuego"
        CType(Me.PBoxFol, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBoxEnglish, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PBoxEnglish As PictureBox
    Friend WithEvents PBoxFol As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblUsuario As Label
    Friend WithEvents lblSalir As Label
End Class
