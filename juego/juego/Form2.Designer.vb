﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PBLogo = New System.Windows.Forms.PictureBox()
        Me.PBCreadores = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PBLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBCreadores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PBLogo
        '
        Me.PBLogo.BackColor = System.Drawing.Color.Transparent
        Me.PBLogo.Image = Global.juego.My.Resources.Resources.LogoReto5
        Me.PBLogo.Location = New System.Drawing.Point(168, 50)
        Me.PBLogo.Name = "PBLogo"
        Me.PBLogo.Size = New System.Drawing.Size(673, 184)
        Me.PBLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBLogo.TabIndex = 0
        Me.PBLogo.TabStop = False
        '
        'PBCreadores
        '
        Me.PBCreadores.BackColor = System.Drawing.Color.Transparent
        Me.PBCreadores.Image = Global.juego.My.Resources.Resources.finjuego
        Me.PBCreadores.Location = New System.Drawing.Point(156, 187)
        Me.PBCreadores.Name = "PBCreadores"
        Me.PBCreadores.Size = New System.Drawing.Size(700, 350)
        Me.PBCreadores.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBCreadores.TabIndex = 1
        Me.PBCreadores.TabStop = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.juego.My.Resources.Resources.fondoFinJuego
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1099, 620)
        Me.Controls.Add(Me.PBCreadores)
        Me.Controls.Add(Me.PBLogo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form2"
        Me.Text = "Form2"
        CType(Me.PBLogo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBCreadores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PBLogo As PictureBox
    Friend WithEvents PBCreadores As PictureBox
    Friend WithEvents Timer1 As Timer
End Class
