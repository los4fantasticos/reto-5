﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pregunta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lblMemoriza = New System.Windows.Forms.Label()
        Me.PBImagen = New System.Windows.Forms.PictureBox()
        Me.lblPregunta = New System.Windows.Forms.Label()
        Me.lblRespuesta1 = New System.Windows.Forms.Label()
        Me.lblRespuesta2 = New System.Windows.Forms.Label()
        Me.lblRespuesta3 = New System.Windows.Forms.Label()
        Me.lblRespuesta4 = New System.Windows.Forms.Label()
        Me.reloj = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblPuntuacion = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblTiempo = New System.Windows.Forms.Label()
        Me.lblRespuestaCorrecta = New System.Windows.Forms.Label()
        Me.lblExplicacion = New System.Windows.Forms.Label()
        Me.lblOmitir = New System.Windows.Forms.Label()
        Me.lblPausar = New System.Windows.Forms.Label()
        Me.lblCorrecto = New System.Windows.Forms.Label()
        Me.PBExplicacion = New System.Windows.Forms.PictureBox()
        Me.LblRC = New System.Windows.Forms.Label()
        Me.LblSeparar = New System.Windows.Forms.Label()
        Me.LblMarcoPregunta = New System.Windows.Forms.Label()
        Me.PBBienMal = New System.Windows.Forms.PictureBox()
        Me.lblSalir = New System.Windows.Forms.Label()
        Me.lblBonusPuntuacion = New System.Windows.Forms.Label()
        Me.lblNumeroPregunta = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        CType(Me.PBImagen, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBExplicacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBBienMal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMemoriza
        '
        Me.lblMemoriza.AutoSize = True
        Me.lblMemoriza.BackColor = System.Drawing.Color.Transparent
        Me.lblMemoriza.Font = New System.Drawing.Font("Tahoma", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMemoriza.ForeColor = System.Drawing.Color.White
        Me.lblMemoriza.Location = New System.Drawing.Point(279, 60)
        Me.lblMemoriza.Name = "lblMemoriza"
        Me.lblMemoriza.Size = New System.Drawing.Size(367, 33)
        Me.lblMemoriza.TabIndex = 1
        Me.lblMemoriza.Text = "MEMORIZA ESTA IMAGEN"
        '
        'PBImagen
        '
        Me.PBImagen.ImageLocation = ""
        Me.PBImagen.Location = New System.Drawing.Point(100, 100)
        Me.PBImagen.Name = "PBImagen"
        Me.PBImagen.Size = New System.Drawing.Size(280, 117)
        Me.PBImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBImagen.TabIndex = 0
        Me.PBImagen.TabStop = False
        '
        'lblPregunta
        '
        Me.lblPregunta.BackColor = System.Drawing.Color.DarkMagenta
        Me.lblPregunta.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPregunta.ForeColor = System.Drawing.Color.White
        Me.lblPregunta.Location = New System.Drawing.Point(227, 125)
        Me.lblPregunta.Name = "lblPregunta"
        Me.lblPregunta.Size = New System.Drawing.Size(485, 70)
        Me.lblPregunta.TabIndex = 3
        Me.lblPregunta.Text = "lblPregunta"
        Me.lblPregunta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblPregunta.Visible = False
        '
        'lblRespuesta1
        '
        Me.lblRespuesta1.BackColor = System.Drawing.Color.Transparent
        Me.lblRespuesta1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblRespuesta1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRespuesta1.Location = New System.Drawing.Point(97, 275)
        Me.lblRespuesta1.Name = "lblRespuesta1"
        Me.lblRespuesta1.Size = New System.Drawing.Size(325, 81)
        Me.lblRespuesta1.TabIndex = 4
        Me.lblRespuesta1.Text = "lblPregunta1"
        Me.lblRespuesta1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblRespuesta1.Visible = False
        '
        'lblRespuesta2
        '
        Me.lblRespuesta2.BackColor = System.Drawing.Color.Transparent
        Me.lblRespuesta2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblRespuesta2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRespuesta2.Location = New System.Drawing.Point(546, 275)
        Me.lblRespuesta2.Name = "lblRespuesta2"
        Me.lblRespuesta2.Size = New System.Drawing.Size(331, 81)
        Me.lblRespuesta2.TabIndex = 5
        Me.lblRespuesta2.Text = "lblPregunta2"
        Me.lblRespuesta2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblRespuesta2.Visible = False
        '
        'lblRespuesta3
        '
        Me.lblRespuesta3.BackColor = System.Drawing.Color.Transparent
        Me.lblRespuesta3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblRespuesta3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRespuesta3.ForeColor = System.Drawing.Color.Black
        Me.lblRespuesta3.Location = New System.Drawing.Point(103, 437)
        Me.lblRespuesta3.Margin = New System.Windows.Forms.Padding(0)
        Me.lblRespuesta3.Name = "lblRespuesta3"
        Me.lblRespuesta3.Size = New System.Drawing.Size(319, 74)
        Me.lblRespuesta3.TabIndex = 6
        Me.lblRespuesta3.Text = "lblPregunta3"
        Me.lblRespuesta3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblRespuesta3.Visible = False
        '
        'lblRespuesta4
        '
        Me.lblRespuesta4.BackColor = System.Drawing.Color.Transparent
        Me.lblRespuesta4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblRespuesta4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRespuesta4.Location = New System.Drawing.Point(552, 437)
        Me.lblRespuesta4.Name = "lblRespuesta4"
        Me.lblRespuesta4.Size = New System.Drawing.Size(325, 74)
        Me.lblRespuesta4.TabIndex = 7
        Me.lblRespuesta4.Text = "lblPregunta4"
        Me.lblRespuesta4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblRespuesta4.Visible = False
        '
        'reloj
        '
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(935, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 19)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Puntuación:"
        '
        'lblPuntuacion
        '
        Me.lblPuntuacion.AutoSize = True
        Me.lblPuntuacion.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion.Font = New System.Drawing.Font("Book Antiqua", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion.ForeColor = System.Drawing.Color.White
        Me.lblPuntuacion.Location = New System.Drawing.Point(982, 128)
        Me.lblPuntuacion.Name = "lblPuntuacion"
        Me.lblPuntuacion.Size = New System.Drawing.Size(23, 26)
        Me.lblPuntuacion.TabIndex = 9
        Me.lblPuntuacion.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(924, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 23)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Tiempo:"
        '
        'lblTiempo
        '
        Me.lblTiempo.AutoSize = True
        Me.lblTiempo.BackColor = System.Drawing.Color.Transparent
        Me.lblTiempo.Font = New System.Drawing.Font("Book Antiqua", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTiempo.ForeColor = System.Drawing.Color.White
        Me.lblTiempo.Location = New System.Drawing.Point(1033, 28)
        Me.lblTiempo.Name = "lblTiempo"
        Me.lblTiempo.Size = New System.Drawing.Size(33, 39)
        Me.lblTiempo.TabIndex = 11
        Me.lblTiempo.Text = "0"
        '
        'lblRespuestaCorrecta
        '
        Me.lblRespuestaCorrecta.BackColor = System.Drawing.Color.Transparent
        Me.lblRespuestaCorrecta.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRespuestaCorrecta.ForeColor = System.Drawing.Color.White
        Me.lblRespuestaCorrecta.Location = New System.Drawing.Point(520, 130)
        Me.lblRespuestaCorrecta.Name = "lblRespuestaCorrecta"
        Me.lblRespuestaCorrecta.Size = New System.Drawing.Size(300, 100)
        Me.lblRespuestaCorrecta.TabIndex = 12
        Me.lblRespuestaCorrecta.Text = "respuesta correcta"
        Me.lblRespuestaCorrecta.Visible = False
        '
        'lblExplicacion
        '
        Me.lblExplicacion.BackColor = System.Drawing.Color.Transparent
        Me.lblExplicacion.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExplicacion.ForeColor = System.Drawing.Color.White
        Me.lblExplicacion.Location = New System.Drawing.Point(520, 275)
        Me.lblExplicacion.Name = "lblExplicacion"
        Me.lblExplicacion.Size = New System.Drawing.Size(300, 285)
        Me.lblExplicacion.TabIndex = 13
        Me.lblExplicacion.Text = "explicacion"
        Me.lblExplicacion.Visible = False
        '
        'lblOmitir
        '
        Me.lblOmitir.BackColor = System.Drawing.Color.Transparent
        Me.lblOmitir.Image = Global.juego.My.Resources.Resources.omitir
        Me.lblOmitir.Location = New System.Drawing.Point(946, 328)
        Me.lblOmitir.Name = "lblOmitir"
        Me.lblOmitir.Size = New System.Drawing.Size(120, 40)
        Me.lblOmitir.TabIndex = 14
        Me.lblOmitir.Visible = False
        '
        'lblPausar
        '
        Me.lblPausar.BackColor = System.Drawing.Color.Transparent
        Me.lblPausar.Font = New System.Drawing.Font("Microsoft Sans Serif", 1.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPausar.ForeColor = System.Drawing.Color.Black
        Me.lblPausar.Image = Global.juego.My.Resources.Resources.pausar2
        Me.lblPausar.Location = New System.Drawing.Point(946, 275)
        Me.lblPausar.Name = "lblPausar"
        Me.lblPausar.Size = New System.Drawing.Size(120, 40)
        Me.lblPausar.TabIndex = 15
        Me.lblPausar.Text = "pausar"
        Me.lblPausar.Visible = False
        '
        'lblCorrecto
        '
        Me.lblCorrecto.AutoSize = True
        Me.lblCorrecto.BackColor = System.Drawing.Color.Transparent
        Me.lblCorrecto.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCorrecto.ForeColor = System.Drawing.Color.White
        Me.lblCorrecto.Location = New System.Drawing.Point(575, 60)
        Me.lblCorrecto.Name = "lblCorrecto"
        Me.lblCorrecto.Size = New System.Drawing.Size(313, 23)
        Me.lblCorrecto.TabIndex = 16
        Me.lblCorrecto.Text = "Respuesta correcta o incorrecta"
        Me.lblCorrecto.Visible = False
        '
        'PBExplicacion
        '
        Me.PBExplicacion.BackColor = System.Drawing.Color.Transparent
        Me.PBExplicacion.Location = New System.Drawing.Point(50, 60)
        Me.PBExplicacion.Name = "PBExplicacion"
        Me.PBExplicacion.Size = New System.Drawing.Size(410, 500)
        Me.PBExplicacion.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBExplicacion.TabIndex = 17
        Me.PBExplicacion.TabStop = False
        Me.PBExplicacion.Visible = False
        '
        'LblRC
        '
        Me.LblRC.AutoSize = True
        Me.LblRC.BackColor = System.Drawing.Color.Transparent
        Me.LblRC.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRC.ForeColor = System.Drawing.Color.White
        Me.LblRC.Location = New System.Drawing.Point(520, 100)
        Me.LblRC.Name = "LblRC"
        Me.LblRC.Size = New System.Drawing.Size(143, 16)
        Me.LblRC.TabIndex = 18
        Me.LblRC.Text = "Respuesta Correcta:"
        Me.LblRC.Visible = False
        '
        'LblSeparar
        '
        Me.LblSeparar.AutoSize = True
        Me.LblSeparar.BackColor = System.Drawing.Color.Transparent
        Me.LblSeparar.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSeparar.ForeColor = System.Drawing.Color.White
        Me.LblSeparar.Location = New System.Drawing.Point(520, 240)
        Me.LblSeparar.Name = "LblSeparar"
        Me.LblSeparar.Size = New System.Drawing.Size(330, 23)
        Me.LblSeparar.TabIndex = 19
        Me.LblSeparar.Text = "===================="
        Me.LblSeparar.Visible = False
        '
        'LblMarcoPregunta
        '
        Me.LblMarcoPregunta.BackColor = System.Drawing.Color.Transparent
        Me.LblMarcoPregunta.Image = Global.juego.My.Resources.Resources.marcoPregunta2
        Me.LblMarcoPregunta.Location = New System.Drawing.Point(207, 115)
        Me.LblMarcoPregunta.Name = "LblMarcoPregunta"
        Me.LblMarcoPregunta.Size = New System.Drawing.Size(529, 91)
        Me.LblMarcoPregunta.TabIndex = 20
        Me.LblMarcoPregunta.Visible = False
        '
        'PBBienMal
        '
        Me.PBBienMal.BackColor = System.Drawing.Color.Transparent
        Me.PBBienMal.Image = Global.juego.My.Resources.Resources.correcto
        Me.PBBienMal.Location = New System.Drawing.Point(466, 17)
        Me.PBBienMal.Name = "PBBienMal"
        Me.PBBienMal.Size = New System.Drawing.Size(103, 85)
        Me.PBBienMal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBBienMal.TabIndex = 21
        Me.PBBienMal.TabStop = False
        Me.PBBienMal.Visible = False
        '
        'lblSalir
        '
        Me.lblSalir.BackColor = System.Drawing.Color.Transparent
        Me.lblSalir.Image = Global.juego.My.Resources.Resources._exit
        Me.lblSalir.Location = New System.Drawing.Point(30, 25)
        Me.lblSalir.Name = "lblSalir"
        Me.lblSalir.Size = New System.Drawing.Size(30, 30)
        Me.lblSalir.TabIndex = 22
        '
        'lblBonusPuntuacion
        '
        Me.lblBonusPuntuacion.AutoSize = True
        Me.lblBonusPuntuacion.BackColor = System.Drawing.Color.Transparent
        Me.lblBonusPuntuacion.Font = New System.Drawing.Font("Book Antiqua", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBonusPuntuacion.ForeColor = System.Drawing.Color.Gold
        Me.lblBonusPuntuacion.Location = New System.Drawing.Point(979, 219)
        Me.lblBonusPuntuacion.Name = "lblBonusPuntuacion"
        Me.lblBonusPuntuacion.Size = New System.Drawing.Size(66, 28)
        Me.lblBonusPuntuacion.TabIndex = 23
        Me.lblBonusPuntuacion.Text = "5.000"
        Me.lblBonusPuntuacion.Visible = False
        '
        'lblNumeroPregunta
        '
        Me.lblNumeroPregunta.AutoSize = True
        Me.lblNumeroPregunta.BackColor = System.Drawing.Color.Transparent
        Me.lblNumeroPregunta.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumeroPregunta.ForeColor = System.Drawing.Color.DarkCyan
        Me.lblNumeroPregunta.Location = New System.Drawing.Point(178, 24)
        Me.lblNumeroPregunta.Name = "lblNumeroPregunta"
        Me.lblNumeroPregunta.Size = New System.Drawing.Size(111, 14)
        Me.lblNumeroPregunta.TabIndex = 24
        Me.lblNumeroPregunta.Text = "numeroPregunta"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.juego.My.Resources.Resources.star
        Me.PictureBox1.Location = New System.Drawing.Point(977, 188)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(19, 18)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 26
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = Global.juego.My.Resources.Resources.star
        Me.PictureBox2.Location = New System.Drawing.Point(1005, 188)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(18, 18)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 27
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Image = Global.juego.My.Resources.Resources.star
        Me.PictureBox3.Location = New System.Drawing.Point(1032, 188)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(18, 18)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 28
        Me.PictureBox3.TabStop = False
        '
        'pregunta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.juego.My.Resources.Resources.fondoFotoOk
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1099, 620)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblNumeroPregunta)
        Me.Controls.Add(Me.lblBonusPuntuacion)
        Me.Controls.Add(Me.lblSalir)
        Me.Controls.Add(Me.PBBienMal)
        Me.Controls.Add(Me.LblSeparar)
        Me.Controls.Add(Me.LblRC)
        Me.Controls.Add(Me.lblCorrecto)
        Me.Controls.Add(Me.lblPausar)
        Me.Controls.Add(Me.lblOmitir)
        Me.Controls.Add(Me.lblExplicacion)
        Me.Controls.Add(Me.lblRespuestaCorrecta)
        Me.Controls.Add(Me.lblTiempo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblPuntuacion)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblRespuesta4)
        Me.Controls.Add(Me.lblRespuesta3)
        Me.Controls.Add(Me.lblPregunta)
        Me.Controls.Add(Me.lblMemoriza)
        Me.Controls.Add(Me.lblRespuesta2)
        Me.Controls.Add(Me.lblRespuesta1)
        Me.Controls.Add(Me.PBExplicacion)
        Me.Controls.Add(Me.LblMarcoPregunta)
        Me.Controls.Add(Me.PBImagen)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "pregunta"
        Me.Text = "pregunta"
        CType(Me.PBImagen, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBExplicacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBBienMal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PBImagen As PictureBox
    Friend WithEvents lblMemoriza As Label
    Friend WithEvents lblPregunta As Label
    Friend WithEvents lblRespuesta1 As Label
    Friend WithEvents lblRespuesta2 As Label
    Friend WithEvents lblRespuesta3 As Label
    Friend WithEvents reloj As Timer
    Friend WithEvents Label1 As Label
    Friend WithEvents lblPuntuacion As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents lblTiempo As Label
    Friend WithEvents lblRespuestaCorrecta As Label
    Friend WithEvents lblExplicacion As Label
    Friend WithEvents lblOmitir As Label
    Friend WithEvents lblPausar As Label
    Friend WithEvents lblCorrecto As Label
    Public WithEvents lblRespuesta4 As Label
    Friend WithEvents PBExplicacion As PictureBox
    Friend WithEvents LblRC As Label
    Friend WithEvents LblSeparar As Label
    Friend WithEvents LblMarcoPregunta As Label
    Friend WithEvents PBBienMal As PictureBox
    Friend WithEvents lblSalir As Label
    Friend WithEvents lblBonusPuntuacion As Label
    Friend WithEvents lblNumeroPregunta As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
End Class
