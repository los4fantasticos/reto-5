﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class pantallaFinal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LabelFinal = New System.Windows.Forms.Label()
        Me.labelP = New System.Windows.Forms.Label()
        Me.lblPuntuacion = New System.Windows.Forms.Label()
        Me.lblRecord = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.labelTopMundial = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.label9 = New System.Windows.Forms.Label()
        Me.label8 = New System.Windows.Forms.Label()
        Me.label7 = New System.Windows.Forms.Label()
        Me.label6 = New System.Windows.Forms.Label()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.labe3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.lblPuntuacion10 = New System.Windows.Forms.Label()
        Me.lblPuntuacion9 = New System.Windows.Forms.Label()
        Me.lblPuntuacion8 = New System.Windows.Forms.Label()
        Me.lblPuntuacion7 = New System.Windows.Forms.Label()
        Me.lblPuntuacion6 = New System.Windows.Forms.Label()
        Me.lblPuntuacion5 = New System.Windows.Forms.Label()
        Me.lblPuntuacion4 = New System.Windows.Forms.Label()
        Me.lblPuntuacion3 = New System.Windows.Forms.Label()
        Me.lblPuntuacion2 = New System.Windows.Forms.Label()
        Me.lblPuntuacion1 = New System.Windows.Forms.Label()
        Me.lblNombre10 = New System.Windows.Forms.Label()
        Me.lblNombre9 = New System.Windows.Forms.Label()
        Me.lblNombre8 = New System.Windows.Forms.Label()
        Me.lblNombre7 = New System.Windows.Forms.Label()
        Me.lblNombre6 = New System.Windows.Forms.Label()
        Me.lblNombre5 = New System.Windows.Forms.Label()
        Me.lblNombre4 = New System.Windows.Forms.Label()
        Me.lblNombre3 = New System.Windows.Forms.Label()
        Me.lblNombre2 = New System.Windows.Forms.Label()
        Me.lblNombre1 = New System.Windows.Forms.Label()
        Me.lbltipo10 = New System.Windows.Forms.Label()
        Me.lbltipo9 = New System.Windows.Forms.Label()
        Me.lbltipo8 = New System.Windows.Forms.Label()
        Me.lbltipo7 = New System.Windows.Forms.Label()
        Me.lbltipo6 = New System.Windows.Forms.Label()
        Me.lbltipo5 = New System.Windows.Forms.Label()
        Me.lbltipo4 = New System.Windows.Forms.Label()
        Me.lbltipo3 = New System.Windows.Forms.Label()
        Me.lbltipo2 = New System.Windows.Forms.Label()
        Me.lbltipo1 = New System.Windows.Forms.Label()
        Me.panelPosicion = New System.Windows.Forms.Panel()
        Me.panelPuntuacion = New System.Windows.Forms.Panel()
        Me.panelNombre = New System.Windows.Forms.Panel()
        Me.panelTipo = New System.Windows.Forms.Panel()
        Me.LblVolverJugar = New System.Windows.Forms.Label()
        Me.LblSalir = New System.Windows.Forms.Label()
        Me.PBMonkey = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.panelPosicion.SuspendLayout()
        Me.panelPuntuacion.SuspendLayout()
        Me.panelNombre.SuspendLayout()
        Me.panelTipo.SuspendLayout()
        CType(Me.PBMonkey, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelFinal
        '
        Me.LabelFinal.BackColor = System.Drawing.Color.Transparent
        Me.LabelFinal.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelFinal.ForeColor = System.Drawing.Color.White
        Me.LabelFinal.Image = Global.juego.My.Resources.Resources.gameover400
        Me.LabelFinal.Location = New System.Drawing.Point(358, 9)
        Me.LabelFinal.Name = "LabelFinal"
        Me.LabelFinal.Size = New System.Drawing.Size(399, 58)
        Me.LabelFinal.TabIndex = 0
        '
        'labelP
        '
        Me.labelP.BackColor = System.Drawing.Color.Transparent
        Me.labelP.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelP.ForeColor = System.Drawing.Color.White
        Me.labelP.Image = Global.juego.My.Resources.Resources.tuPuntuacion200
        Me.labelP.Location = New System.Drawing.Point(384, 81)
        Me.labelP.Name = "labelP"
        Me.labelP.Size = New System.Drawing.Size(198, 29)
        Me.labelP.TabIndex = 1
        '
        'lblPuntuacion
        '
        Me.lblPuntuacion.AutoSize = True
        Me.lblPuntuacion.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion.ForeColor = System.Drawing.Color.Orange
        Me.lblPuntuacion.Location = New System.Drawing.Point(614, 81)
        Me.lblPuntuacion.Name = "lblPuntuacion"
        Me.lblPuntuacion.Size = New System.Drawing.Size(43, 29)
        Me.lblPuntuacion.TabIndex = 2
        Me.lblPuntuacion.Text = "00"
        '
        'lblRecord
        '
        Me.lblRecord.AutoSize = True
        Me.lblRecord.BackColor = System.Drawing.Color.Transparent
        Me.lblRecord.Font = New System.Drawing.Font("Stencil", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRecord.ForeColor = System.Drawing.Color.White
        Me.lblRecord.Location = New System.Drawing.Point(850, 80)
        Me.lblRecord.Name = "lblRecord"
        Me.lblRecord.Size = New System.Drawing.Size(237, 34)
        Me.lblRecord.TabIndex = 3
        Me.lblRecord.Text = "NUEVO RECORD"
        Me.lblRecord.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(61, 233)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(0, 25)
        Me.Label3.TabIndex = 4
        '
        'labelTopMundial
        '
        Me.labelTopMundial.BackColor = System.Drawing.Color.Transparent
        Me.labelTopMundial.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labelTopMundial.ForeColor = System.Drawing.Color.White
        Me.labelTopMundial.Image = Global.juego.My.Resources.Resources.hi_scores_300
        Me.labelTopMundial.Location = New System.Drawing.Point(386, 143)
        Me.labelTopMundial.Name = "labelTopMundial"
        Me.labelTopMundial.Size = New System.Drawing.Size(301, 54)
        Me.labelTopMundial.TabIndex = 5
        '
        'label10
        '
        Me.label10.AutoSize = True
        Me.label10.BackColor = System.Drawing.Color.Transparent
        Me.label10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label10.ForeColor = System.Drawing.Color.White
        Me.label10.Location = New System.Drawing.Point(5, 354)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(29, 19)
        Me.label10.TabIndex = 6
        Me.label10.Text = "10"
        Me.label10.Visible = False
        '
        'label9
        '
        Me.label9.AutoSize = True
        Me.label9.BackColor = System.Drawing.Color.Transparent
        Me.label9.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label9.ForeColor = System.Drawing.Color.White
        Me.label9.Location = New System.Drawing.Point(15, 318)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(19, 19)
        Me.label9.TabIndex = 7
        Me.label9.Text = "9"
        Me.label9.Visible = False
        '
        'label8
        '
        Me.label8.AutoSize = True
        Me.label8.BackColor = System.Drawing.Color.Transparent
        Me.label8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label8.ForeColor = System.Drawing.Color.White
        Me.label8.Location = New System.Drawing.Point(15, 280)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(19, 19)
        Me.label8.TabIndex = 8
        Me.label8.Text = "8"
        Me.label8.Visible = False
        '
        'label7
        '
        Me.label7.AutoSize = True
        Me.label7.BackColor = System.Drawing.Color.Transparent
        Me.label7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label7.ForeColor = System.Drawing.Color.White
        Me.label7.Location = New System.Drawing.Point(15, 243)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(19, 19)
        Me.label7.TabIndex = 9
        Me.label7.Text = "7"
        Me.label7.Visible = False
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.BackColor = System.Drawing.Color.Transparent
        Me.label6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label6.ForeColor = System.Drawing.Color.White
        Me.label6.Location = New System.Drawing.Point(15, 206)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(19, 19)
        Me.label6.TabIndex = 10
        Me.label6.Text = "6"
        Me.label6.Visible = False
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.BackColor = System.Drawing.Color.Transparent
        Me.label5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.ForeColor = System.Drawing.Color.White
        Me.label5.Location = New System.Drawing.Point(15, 169)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(19, 19)
        Me.label5.TabIndex = 11
        Me.label5.Text = "5"
        Me.label5.Visible = False
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.BackColor = System.Drawing.Color.Transparent
        Me.label4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label4.ForeColor = System.Drawing.Color.White
        Me.label4.Location = New System.Drawing.Point(15, 132)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(19, 19)
        Me.label4.TabIndex = 12
        Me.label4.Text = "4"
        Me.label4.Visible = False
        '
        'labe3
        '
        Me.labe3.AutoSize = True
        Me.labe3.BackColor = System.Drawing.Color.Transparent
        Me.labe3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labe3.ForeColor = System.Drawing.Color.White
        Me.labe3.Location = New System.Drawing.Point(15, 94)
        Me.labe3.Name = "labe3"
        Me.labe3.Size = New System.Drawing.Size(19, 19)
        Me.labe3.TabIndex = 13
        Me.labe3.Text = "3"
        Me.labe3.Visible = False
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.BackColor = System.Drawing.Color.Transparent
        Me.label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.ForeColor = System.Drawing.Color.White
        Me.label2.Location = New System.Drawing.Point(15, 57)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(19, 19)
        Me.label2.TabIndex = 14
        Me.label2.Text = "2"
        Me.label2.Visible = False
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.BackColor = System.Drawing.Color.Transparent
        Me.label1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.ForeColor = System.Drawing.Color.White
        Me.label1.Location = New System.Drawing.Point(15, 20)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(19, 19)
        Me.label1.TabIndex = 15
        Me.label1.Text = "1"
        Me.label1.Visible = False
        '
        'lblPuntuacion10
        '
        Me.lblPuntuacion10.AutoSize = True
        Me.lblPuntuacion10.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion10.Location = New System.Drawing.Point(13, 354)
        Me.lblPuntuacion10.Name = "lblPuntuacion10"
        Me.lblPuntuacion10.Size = New System.Drawing.Size(73, 19)
        Me.lblPuntuacion10.TabIndex = 16
        Me.lblPuntuacion10.Text = "Label10"
        Me.lblPuntuacion10.Visible = False
        '
        'lblPuntuacion9
        '
        Me.lblPuntuacion9.AutoSize = True
        Me.lblPuntuacion9.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion9.ForeColor = System.Drawing.Color.White
        Me.lblPuntuacion9.Location = New System.Drawing.Point(13, 318)
        Me.lblPuntuacion9.Name = "lblPuntuacion9"
        Me.lblPuntuacion9.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion9.TabIndex = 17
        Me.lblPuntuacion9.Text = "Label9"
        Me.lblPuntuacion9.Visible = False
        '
        'lblPuntuacion8
        '
        Me.lblPuntuacion8.AutoSize = True
        Me.lblPuntuacion8.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion8.Location = New System.Drawing.Point(13, 280)
        Me.lblPuntuacion8.Name = "lblPuntuacion8"
        Me.lblPuntuacion8.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion8.TabIndex = 18
        Me.lblPuntuacion8.Text = "Label8"
        Me.lblPuntuacion8.Visible = False
        '
        'lblPuntuacion7
        '
        Me.lblPuntuacion7.AutoSize = True
        Me.lblPuntuacion7.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion7.Location = New System.Drawing.Point(13, 243)
        Me.lblPuntuacion7.Name = "lblPuntuacion7"
        Me.lblPuntuacion7.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion7.TabIndex = 19
        Me.lblPuntuacion7.Text = "Label7"
        Me.lblPuntuacion7.Visible = False
        '
        'lblPuntuacion6
        '
        Me.lblPuntuacion6.AutoSize = True
        Me.lblPuntuacion6.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion6.Location = New System.Drawing.Point(13, 206)
        Me.lblPuntuacion6.Name = "lblPuntuacion6"
        Me.lblPuntuacion6.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion6.TabIndex = 20
        Me.lblPuntuacion6.Text = "Label6"
        Me.lblPuntuacion6.Visible = False
        '
        'lblPuntuacion5
        '
        Me.lblPuntuacion5.AutoSize = True
        Me.lblPuntuacion5.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion5.Location = New System.Drawing.Point(13, 169)
        Me.lblPuntuacion5.Name = "lblPuntuacion5"
        Me.lblPuntuacion5.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion5.TabIndex = 21
        Me.lblPuntuacion5.Text = "Label5"
        Me.lblPuntuacion5.Visible = False
        '
        'lblPuntuacion4
        '
        Me.lblPuntuacion4.AutoSize = True
        Me.lblPuntuacion4.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion4.Location = New System.Drawing.Point(13, 132)
        Me.lblPuntuacion4.Name = "lblPuntuacion4"
        Me.lblPuntuacion4.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion4.TabIndex = 22
        Me.lblPuntuacion4.Text = "Label4"
        Me.lblPuntuacion4.Visible = False
        '
        'lblPuntuacion3
        '
        Me.lblPuntuacion3.AutoSize = True
        Me.lblPuntuacion3.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion3.Location = New System.Drawing.Point(13, 94)
        Me.lblPuntuacion3.Name = "lblPuntuacion3"
        Me.lblPuntuacion3.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion3.TabIndex = 23
        Me.lblPuntuacion3.Text = "Label3"
        Me.lblPuntuacion3.Visible = False
        '
        'lblPuntuacion2
        '
        Me.lblPuntuacion2.AutoSize = True
        Me.lblPuntuacion2.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion2.Location = New System.Drawing.Point(13, 57)
        Me.lblPuntuacion2.Name = "lblPuntuacion2"
        Me.lblPuntuacion2.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion2.TabIndex = 24
        Me.lblPuntuacion2.Text = "Label2"
        Me.lblPuntuacion2.Visible = False
        '
        'lblPuntuacion1
        '
        Me.lblPuntuacion1.AutoSize = True
        Me.lblPuntuacion1.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion1.Location = New System.Drawing.Point(13, 20)
        Me.lblPuntuacion1.Name = "lblPuntuacion1"
        Me.lblPuntuacion1.Size = New System.Drawing.Size(63, 19)
        Me.lblPuntuacion1.TabIndex = 25
        Me.lblPuntuacion1.Text = "Label1"
        Me.lblPuntuacion1.Visible = False
        '
        'lblNombre10
        '
        Me.lblNombre10.AutoSize = True
        Me.lblNombre10.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre10.ForeColor = System.Drawing.Color.White
        Me.lblNombre10.Location = New System.Drawing.Point(3, 354)
        Me.lblNombre10.Name = "lblNombre10"
        Me.lblNombre10.Size = New System.Drawing.Size(93, 19)
        Me.lblNombre10.TabIndex = 26
        Me.lblNombre10.Text = "Nombre10"
        Me.lblNombre10.Visible = False
        '
        'lblNombre9
        '
        Me.lblNombre9.AutoSize = True
        Me.lblNombre9.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre9.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre9.ForeColor = System.Drawing.Color.White
        Me.lblNombre9.Location = New System.Drawing.Point(3, 318)
        Me.lblNombre9.Name = "lblNombre9"
        Me.lblNombre9.Size = New System.Drawing.Size(83, 19)
        Me.lblNombre9.TabIndex = 27
        Me.lblNombre9.Text = "Nombre9"
        Me.lblNombre9.Visible = False
        '
        'lblNombre8
        '
        Me.lblNombre8.AutoSize = True
        Me.lblNombre8.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre8.ForeColor = System.Drawing.Color.White
        Me.lblNombre8.Location = New System.Drawing.Point(3, 280)
        Me.lblNombre8.Name = "lblNombre8"
        Me.lblNombre8.Size = New System.Drawing.Size(83, 19)
        Me.lblNombre8.TabIndex = 28
        Me.lblNombre8.Text = "Nombre8"
        Me.lblNombre8.Visible = False
        '
        'lblNombre7
        '
        Me.lblNombre7.AutoSize = True
        Me.lblNombre7.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre7.ForeColor = System.Drawing.Color.White
        Me.lblNombre7.Location = New System.Drawing.Point(3, 243)
        Me.lblNombre7.Name = "lblNombre7"
        Me.lblNombre7.Size = New System.Drawing.Size(83, 19)
        Me.lblNombre7.TabIndex = 29
        Me.lblNombre7.Text = "Nombre7"
        Me.lblNombre7.Visible = False
        '
        'lblNombre6
        '
        Me.lblNombre6.AutoSize = True
        Me.lblNombre6.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre6.ForeColor = System.Drawing.Color.White
        Me.lblNombre6.Location = New System.Drawing.Point(3, 206)
        Me.lblNombre6.Name = "lblNombre6"
        Me.lblNombre6.Size = New System.Drawing.Size(83, 19)
        Me.lblNombre6.TabIndex = 30
        Me.lblNombre6.Text = "Nombre6"
        Me.lblNombre6.Visible = False
        '
        'lblNombre5
        '
        Me.lblNombre5.AutoSize = True
        Me.lblNombre5.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre5.ForeColor = System.Drawing.Color.White
        Me.lblNombre5.Location = New System.Drawing.Point(3, 169)
        Me.lblNombre5.Name = "lblNombre5"
        Me.lblNombre5.Size = New System.Drawing.Size(83, 19)
        Me.lblNombre5.TabIndex = 31
        Me.lblNombre5.Text = "Nombre5"
        Me.lblNombre5.Visible = False
        '
        'lblNombre4
        '
        Me.lblNombre4.AutoSize = True
        Me.lblNombre4.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre4.ForeColor = System.Drawing.Color.White
        Me.lblNombre4.Location = New System.Drawing.Point(3, 132)
        Me.lblNombre4.Name = "lblNombre4"
        Me.lblNombre4.Size = New System.Drawing.Size(83, 19)
        Me.lblNombre4.TabIndex = 32
        Me.lblNombre4.Text = "Nombre4"
        Me.lblNombre4.Visible = False
        '
        'lblNombre3
        '
        Me.lblNombre3.AutoSize = True
        Me.lblNombre3.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre3.ForeColor = System.Drawing.Color.White
        Me.lblNombre3.Location = New System.Drawing.Point(3, 94)
        Me.lblNombre3.Name = "lblNombre3"
        Me.lblNombre3.Size = New System.Drawing.Size(83, 19)
        Me.lblNombre3.TabIndex = 33
        Me.lblNombre3.Text = "Nombre3"
        Me.lblNombre3.Visible = False
        '
        'lblNombre2
        '
        Me.lblNombre2.AutoSize = True
        Me.lblNombre2.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre2.ForeColor = System.Drawing.Color.White
        Me.lblNombre2.Location = New System.Drawing.Point(3, 57)
        Me.lblNombre2.Name = "lblNombre2"
        Me.lblNombre2.Size = New System.Drawing.Size(83, 19)
        Me.lblNombre2.TabIndex = 34
        Me.lblNombre2.Text = "Nombre2"
        Me.lblNombre2.Visible = False
        '
        'lblNombre1
        '
        Me.lblNombre1.AutoSize = True
        Me.lblNombre1.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre1.ForeColor = System.Drawing.Color.White
        Me.lblNombre1.Location = New System.Drawing.Point(3, 20)
        Me.lblNombre1.Name = "lblNombre1"
        Me.lblNombre1.Size = New System.Drawing.Size(81, 20)
        Me.lblNombre1.TabIndex = 35
        Me.lblNombre1.Text = "Nombre1"
        Me.lblNombre1.Visible = False
        '
        'lbltipo10
        '
        Me.lbltipo10.AutoSize = True
        Me.lbltipo10.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo10.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo10.ForeColor = System.Drawing.Color.White
        Me.lbltipo10.Location = New System.Drawing.Point(10, 354)
        Me.lbltipo10.Name = "lbltipo10"
        Me.lbltipo10.Size = New System.Drawing.Size(61, 19)
        Me.lbltipo10.TabIndex = 36
        Me.lbltipo10.Text = "tipo10"
        Me.lbltipo10.Visible = False
        '
        'lbltipo9
        '
        Me.lbltipo9.AutoSize = True
        Me.lbltipo9.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo9.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo9.ForeColor = System.Drawing.Color.White
        Me.lbltipo9.Location = New System.Drawing.Point(10, 318)
        Me.lbltipo9.Name = "lbltipo9"
        Me.lbltipo9.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo9.TabIndex = 37
        Me.lbltipo9.Text = "tipo9"
        Me.lbltipo9.Visible = False
        '
        'lbltipo8
        '
        Me.lbltipo8.AutoSize = True
        Me.lbltipo8.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo8.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo8.ForeColor = System.Drawing.Color.White
        Me.lbltipo8.Location = New System.Drawing.Point(10, 280)
        Me.lbltipo8.Name = "lbltipo8"
        Me.lbltipo8.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo8.TabIndex = 38
        Me.lbltipo8.Text = "tipo8"
        Me.lbltipo8.Visible = False
        '
        'lbltipo7
        '
        Me.lbltipo7.AutoSize = True
        Me.lbltipo7.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo7.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo7.ForeColor = System.Drawing.Color.White
        Me.lbltipo7.Location = New System.Drawing.Point(10, 243)
        Me.lbltipo7.Name = "lbltipo7"
        Me.lbltipo7.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo7.TabIndex = 39
        Me.lbltipo7.Text = "tipo7"
        Me.lbltipo7.Visible = False
        '
        'lbltipo6
        '
        Me.lbltipo6.AutoSize = True
        Me.lbltipo6.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo6.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo6.ForeColor = System.Drawing.Color.White
        Me.lbltipo6.Location = New System.Drawing.Point(10, 206)
        Me.lbltipo6.Name = "lbltipo6"
        Me.lbltipo6.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo6.TabIndex = 40
        Me.lbltipo6.Text = "tipo6"
        Me.lbltipo6.Visible = False
        '
        'lbltipo5
        '
        Me.lbltipo5.AutoSize = True
        Me.lbltipo5.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo5.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo5.ForeColor = System.Drawing.Color.White
        Me.lbltipo5.Location = New System.Drawing.Point(10, 169)
        Me.lbltipo5.Name = "lbltipo5"
        Me.lbltipo5.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo5.TabIndex = 41
        Me.lbltipo5.Text = "tipo5"
        Me.lbltipo5.Visible = False
        '
        'lbltipo4
        '
        Me.lbltipo4.AutoSize = True
        Me.lbltipo4.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo4.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo4.ForeColor = System.Drawing.Color.White
        Me.lbltipo4.Location = New System.Drawing.Point(10, 132)
        Me.lbltipo4.Name = "lbltipo4"
        Me.lbltipo4.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo4.TabIndex = 42
        Me.lbltipo4.Text = "tipo4"
        Me.lbltipo4.Visible = False
        '
        'lbltipo3
        '
        Me.lbltipo3.AutoSize = True
        Me.lbltipo3.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo3.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo3.ForeColor = System.Drawing.Color.White
        Me.lbltipo3.Location = New System.Drawing.Point(10, 94)
        Me.lbltipo3.Name = "lbltipo3"
        Me.lbltipo3.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo3.TabIndex = 43
        Me.lbltipo3.Text = "tipo3"
        Me.lbltipo3.Visible = False
        '
        'lbltipo2
        '
        Me.lbltipo2.AutoSize = True
        Me.lbltipo2.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo2.ForeColor = System.Drawing.Color.White
        Me.lbltipo2.Location = New System.Drawing.Point(10, 57)
        Me.lbltipo2.Name = "lbltipo2"
        Me.lbltipo2.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo2.TabIndex = 44
        Me.lbltipo2.Text = "tipo2"
        Me.lbltipo2.Visible = False
        '
        'lbltipo1
        '
        Me.lbltipo1.AutoSize = True
        Me.lbltipo1.BackColor = System.Drawing.Color.Transparent
        Me.lbltipo1.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltipo1.ForeColor = System.Drawing.Color.White
        Me.lbltipo1.Location = New System.Drawing.Point(10, 20)
        Me.lbltipo1.Name = "lbltipo1"
        Me.lbltipo1.Size = New System.Drawing.Size(51, 19)
        Me.lbltipo1.TabIndex = 45
        Me.lbltipo1.Text = "tipo1"
        Me.lbltipo1.Visible = False
        '
        'panelPosicion
        '
        Me.panelPosicion.BackColor = System.Drawing.Color.Transparent
        Me.panelPosicion.Controls.Add(Me.label1)
        Me.panelPosicion.Controls.Add(Me.label2)
        Me.panelPosicion.Controls.Add(Me.labe3)
        Me.panelPosicion.Controls.Add(Me.label4)
        Me.panelPosicion.Controls.Add(Me.label5)
        Me.panelPosicion.Controls.Add(Me.label6)
        Me.panelPosicion.Controls.Add(Me.label7)
        Me.panelPosicion.Controls.Add(Me.label8)
        Me.panelPosicion.Controls.Add(Me.label9)
        Me.panelPosicion.Controls.Add(Me.label10)
        Me.panelPosicion.Location = New System.Drawing.Point(285, 199)
        Me.panelPosicion.Name = "panelPosicion"
        Me.panelPosicion.Size = New System.Drawing.Size(62, 396)
        Me.panelPosicion.TabIndex = 46
        '
        'panelPuntuacion
        '
        Me.panelPuntuacion.BackColor = System.Drawing.Color.Transparent
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion1)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion2)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion3)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion4)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion5)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion6)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion7)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion8)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion9)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion10)
        Me.panelPuntuacion.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panelPuntuacion.ForeColor = System.Drawing.Color.White
        Me.panelPuntuacion.Location = New System.Drawing.Point(389, 199)
        Me.panelPuntuacion.Name = "panelPuntuacion"
        Me.panelPuntuacion.Size = New System.Drawing.Size(89, 396)
        Me.panelPuntuacion.TabIndex = 47
        '
        'panelNombre
        '
        Me.panelNombre.BackColor = System.Drawing.Color.Transparent
        Me.panelNombre.Controls.Add(Me.lblNombre1)
        Me.panelNombre.Controls.Add(Me.lblNombre2)
        Me.panelNombre.Controls.Add(Me.lblNombre3)
        Me.panelNombre.Controls.Add(Me.lblNombre4)
        Me.panelNombre.Controls.Add(Me.lblNombre5)
        Me.panelNombre.Controls.Add(Me.lblNombre6)
        Me.panelNombre.Controls.Add(Me.lblNombre7)
        Me.panelNombre.Controls.Add(Me.lblNombre8)
        Me.panelNombre.Controls.Add(Me.lblNombre9)
        Me.panelNombre.Controls.Add(Me.lblNombre10)
        Me.panelNombre.Location = New System.Drawing.Point(523, 199)
        Me.panelNombre.Name = "panelNombre"
        Me.panelNombre.Size = New System.Drawing.Size(104, 396)
        Me.panelNombre.TabIndex = 48
        '
        'panelTipo
        '
        Me.panelTipo.BackColor = System.Drawing.Color.Transparent
        Me.panelTipo.Controls.Add(Me.lbltipo1)
        Me.panelTipo.Controls.Add(Me.lbltipo2)
        Me.panelTipo.Controls.Add(Me.lbltipo3)
        Me.panelTipo.Controls.Add(Me.lbltipo4)
        Me.panelTipo.Controls.Add(Me.lbltipo5)
        Me.panelTipo.Controls.Add(Me.lbltipo6)
        Me.panelTipo.Controls.Add(Me.lbltipo7)
        Me.panelTipo.Controls.Add(Me.lbltipo8)
        Me.panelTipo.Controls.Add(Me.lbltipo9)
        Me.panelTipo.Controls.Add(Me.lbltipo10)
        Me.panelTipo.Location = New System.Drawing.Point(686, 199)
        Me.panelTipo.Name = "panelTipo"
        Me.panelTipo.Size = New System.Drawing.Size(99, 396)
        Me.panelTipo.TabIndex = 49
        '
        'LblVolverJugar
        '
        Me.LblVolverJugar.BackColor = System.Drawing.Color.Transparent
        Me.LblVolverJugar.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblVolverJugar.ForeColor = System.Drawing.Color.White
        Me.LblVolverJugar.Image = Global.juego.My.Resources.Resources.Replay1OK
        Me.LblVolverJugar.Location = New System.Drawing.Point(899, 301)
        Me.LblVolverJugar.Name = "LblVolverJugar"
        Me.LblVolverJugar.Size = New System.Drawing.Size(170, 86)
        Me.LblVolverJugar.TabIndex = 50
        '
        'LblSalir
        '
        Me.LblSalir.BackColor = System.Drawing.Color.Transparent
        Me.LblSalir.Font = New System.Drawing.Font("Tahoma", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSalir.ForeColor = System.Drawing.Color.White
        Me.LblSalir.Image = Global.juego.My.Resources.Resources.Exit1OK
        Me.LblSalir.Location = New System.Drawing.Point(934, 375)
        Me.LblSalir.Name = "LblSalir"
        Me.LblSalir.Size = New System.Drawing.Size(86, 86)
        Me.LblSalir.TabIndex = 51
        '
        'PBMonkey
        '
        Me.PBMonkey.BackColor = System.Drawing.Color.Transparent
        Me.PBMonkey.Image = Global.juego.My.Resources.Resources.monoRecord
        Me.PBMonkey.Location = New System.Drawing.Point(904, 130)
        Me.PBMonkey.Name = "PBMonkey"
        Me.PBMonkey.Size = New System.Drawing.Size(147, 98)
        Me.PBMonkey.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBMonkey.TabIndex = 52
        Me.PBMonkey.TabStop = False
        Me.PBMonkey.Visible = False
        '
        'Timer1
        '
        '
        'pantallaFinal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.juego.My.Resources.Resources.fondoPantallaFinal
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1099, 620)
        Me.Controls.Add(Me.panelTipo)
        Me.Controls.Add(Me.panelPosicion)
        Me.Controls.Add(Me.PBMonkey)
        Me.Controls.Add(Me.LblSalir)
        Me.Controls.Add(Me.LblVolverJugar)
        Me.Controls.Add(Me.labelTopMundial)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblRecord)
        Me.Controls.Add(Me.lblPuntuacion)
        Me.Controls.Add(Me.panelPuntuacion)
        Me.Controls.Add(Me.panelNombre)
        Me.Controls.Add(Me.labelP)
        Me.Controls.Add(Me.LabelFinal)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "pantallaFinal"
        Me.Text = "pantallaFinal"
        Me.panelPosicion.ResumeLayout(False)
        Me.panelPosicion.PerformLayout()
        Me.panelPuntuacion.ResumeLayout(False)
        Me.panelPuntuacion.PerformLayout()
        Me.panelNombre.ResumeLayout(False)
        Me.panelNombre.PerformLayout()
        Me.panelTipo.ResumeLayout(False)
        Me.panelTipo.PerformLayout()
        CType(Me.PBMonkey, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelFinal As Label
    Friend WithEvents labelP As Label
    Friend WithEvents lblPuntuacion As Label
    Friend WithEvents lblRecord As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents labelTopMundial As Label
    Friend WithEvents label10 As Label
    Friend WithEvents label9 As Label
    Friend WithEvents label8 As Label
    Friend WithEvents label7 As Label
    Friend WithEvents label6 As Label
    Friend WithEvents label5 As Label
    Friend WithEvents label4 As Label
    Friend WithEvents labe3 As Label
    Friend WithEvents label2 As Label
    Friend WithEvents label1 As Label
    Friend WithEvents lblPuntuacion10 As Label
    Friend WithEvents lblPuntuacion9 As Label
    Friend WithEvents lblPuntuacion8 As Label
    Friend WithEvents lblPuntuacion7 As Label
    Friend WithEvents lblPuntuacion6 As Label
    Friend WithEvents lblPuntuacion5 As Label
    Friend WithEvents lblPuntuacion4 As Label
    Friend WithEvents lblPuntuacion3 As Label
    Friend WithEvents lblPuntuacion2 As Label
    Friend WithEvents lblPuntuacion1 As Label
    Friend WithEvents lblNombre10 As Label
    Friend WithEvents lblNombre9 As Label
    Friend WithEvents lblNombre8 As Label
    Friend WithEvents lblNombre7 As Label
    Friend WithEvents lblNombre6 As Label
    Friend WithEvents lblNombre5 As Label
    Friend WithEvents lblNombre4 As Label
    Friend WithEvents lblNombre3 As Label
    Friend WithEvents lblNombre2 As Label
    Friend WithEvents lblNombre1 As Label
    Friend WithEvents lbltipo10 As Label
    Friend WithEvents lbltipo9 As Label
    Friend WithEvents lbltipo8 As Label
    Friend WithEvents lbltipo7 As Label
    Friend WithEvents lbltipo6 As Label
    Friend WithEvents lbltipo5 As Label
    Friend WithEvents lbltipo4 As Label
    Friend WithEvents lbltipo3 As Label
    Friend WithEvents lbltipo2 As Label
    Friend WithEvents lbltipo1 As Label
    Friend WithEvents panelPosicion As Panel
    Friend WithEvents panelPuntuacion As Panel
    Friend WithEvents panelNombre As Panel
    Friend WithEvents panelTipo As Panel
    Friend WithEvents LblVolverJugar As Label
    Friend WithEvents LblSalir As Label
    Friend WithEvents PBMonkey As PictureBox
    Friend WithEvents Timer1 As Timer
End Class
