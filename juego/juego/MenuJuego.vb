﻿Imports System.Net
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports Oracle.ManagedDataAccess.Client
Public Class MenuJuego
    Public preguntas As Object
    Dim json As String

    Dim arrayPosicion As ArrayList
    Dim arrayPuntuacion As ArrayList
    Dim arrayNombres As ArrayList

    Dim arrayPosicionJugador As ArrayList
    Dim arrayPuntuacionJugador As ArrayList
    Dim arrayFechasJugador As ArrayList

    Dim dr, dr2 As OracleDataReader



    Private Sub MenuJuego_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If (TipoJuego.tipo = "ingles") Then

            LblVolver.Image = My.Resources.Back1

            LblNuevaPartida.Image = My.Resources.NewGame1
        Else

            LblVolver.Image = My.Resources.volver1

            LblNuevaPartida.Image = My.Resources.nuevapartida1
        End If




        Me.Location = TipoJuego.Location
        Me.Size = Form1.Size



        If (TipoJuego.tipo = "ingles") Then
            Label1.Text = "Best Scores:"
            Label2.Text = "My Scores:"
        Else
            Label1.Text = "Mejores Puntuaciones:"
            Label2.Text = "Mis Puntuaciones:"
        End If
        arrayPosicion = New ArrayList
        arrayPuntuacion = New ArrayList
        arrayNombres = New ArrayList

        arrayPosicionJugador = New ArrayList
        arrayPuntuacionJugador = New ArrayList
        arrayFechasJugador = New ArrayList

        For Each p As Label In panelPosicion.Controls
            arrayPosicion.Add(p)

        Next
        For Each p As Label In panelPuntuacion.Controls
            arrayPuntuacion.Add(p)


        Next
        For Each p As Label In panelNombre.Controls
            arrayNombres.Add(p)

        Next

        For Each p As Label In panelPosicionJugador.Controls
            arrayPosicionJugador.Add(p)


        Next
        For Each p As Label In panelPuntuacionJugador.Controls
            arrayPuntuacionJugador.Add(p)



        Next
        For Each p As Label In panelFechasJugador.Controls
            arrayFechasJugador.Add(p)


        Next

        cargarTopTenGeneral()
        cargarTopTenJugador()



    End Sub

    Private Sub LblNuevaPartida_Click(sender As Object, e As EventArgs) Handles LblNuevaPartida.Click

        My.Computer.Audio.Stop()

        Dim url As String = "http://83.213.11.23:8080/api/preguntas/" & TipoJuego.tipo

        Try

            Dim client = New WebClient()
            client.Encoding = System.Text.Encoding.UTF8
            json = client.DownloadString(url)
        Catch ex As Exception
            MsgBox("Problemas al conectar con el servidor , por favor intentelo mas tarde")
        End Try


        Try
            Dim respuesta = JsonConvert.DeserializeObject(json)
            preguntas = respuesta("data")
        Catch ex As Exception
            MsgBox("error al cargar los datos, formato de datos incorrectos")
        End Try

        Me.Hide()
        pregunta.Show()
    End Sub

    Private Sub LblNuevaPartida_MouseHover(sender As Object, e As EventArgs) Handles LblNuevaPartida.MouseHover
        If (TipoJuego.tipo = "ingles") Then

            LblNuevaPartida.Image = My.Resources.NewGame2
        Else

            LblNuevaPartida.Image = My.Resources.nuevapartida2
        End If


    End Sub

    Private Sub LblNuevaPartida_MouseLeave(sender As Object, e As EventArgs) Handles LblNuevaPartida.MouseLeave
        If (TipoJuego.tipo = "ingles") Then

            LblNuevaPartida.Image = My.Resources.NewGame1
        Else

            LblNuevaPartida.Image = My.Resources.nuevapartida1
        End If


    End Sub

    Private Sub LblVolver_Click(sender As Object, e As EventArgs) Handles LblVolver.Click
        Me.Close()
        TipoJuego.Show()
    End Sub

    Private Sub LblVolver_MouseHover(sender As Object, e As EventArgs) Handles LblVolver.MouseHover
        If (TipoJuego.tipo = "ingles") Then

            LblVolver.Image = My.Resources.Back2
        Else

            LblVolver.Image = My.Resources.volver2
        End If

    End Sub

    Private Sub LblVolver_MouseLeave(sender As Object, e As EventArgs) Handles LblVolver.MouseLeave
        If (TipoJuego.tipo = "ingles") Then

            LblVolver.Image = My.Resources.Back1
        Else

            LblVolver.Image = My.Resources.volver1
            LblVolver.Image = My.Resources.volver1
        End If

    End Sub


    Private Sub cargarTopTenJugador()

        Dim command As OracleCommand
        command = New OracleCommand("select sum(score) as puntuacion,fecha,usuarios.nombre as nombreUsuario,tipo_juego.nombre 
from pregunpar inner join partida on partida.id_partida=pregunpar.id_partida 
inner join usuarios on usuarios.id_usuario=partida.id_usuario 
inner join tipo_juego on partida.id_tipo_juego=tipo_juego.id_tipo_juego 
where tipo_juego.id_tipo_juego= " & Val(TipoJuego.id_tipo_juego) & " 
and usuarios.id_usuario= " & Form1.idUsuario & "
 group by partida.id_partida , fecha,usuarios.nombre,tipo_juego.nombre order by puntuacion desc FETCH FIRST 10 ROWS ONLY", Form1.oracleConnection1)
        Try

            dr = command.ExecuteReader()

            If dr.HasRows Then

                llenarLabelsJugador()
            End If

        Catch ex As Exception
            MsgBox("error al conectar con la base de datos , intentelo mas tarde por favor")
        End Try
    End Sub

    Private Sub llenarLabelsJugador()
        Dim cont As Integer = 0
        While dr.Read()
            arrayPosicionJugador(cont).visible = True
            arrayPuntuacionJugador(cont).visible = True
            arrayFechasJugador(cont).visible = True


            arrayPuntuacionJugador(cont).text = dr("puntuacion")
            arrayFechasJugador(cont).text = dr("fecha")



            cont += 1
        End While
    End Sub

    Private Sub cargarTopTenGeneral()


        Dim command As OracleCommand
        command = New OracleCommand("select sum(score) as puntuacion,fecha,usuarios.nombre as nombreUsuario,tipo_juego.nombre 
from pregunpar inner join partida on partida.id_partida=pregunpar.id_partida 
inner join usuarios on usuarios.id_usuario=partida.id_usuario 
inner join tipo_juego on partida.id_tipo_juego=tipo_juego.id_tipo_juego 
where tipo_juego.id_tipo_juego= " & Val(TipoJuego.id_tipo_juego) & " 
 group by partida.id_partida , fecha,usuarios.nombre,tipo_juego.nombre order by puntuacion desc FETCH FIRST 10 ROWS ONLY", Form1.oracleConnection1)
        Try

            dr2 = command.ExecuteReader()


            If dr2.HasRows Then

                llenarLabelsTopGeneral()


            End If
        Catch ex As Exception
            MsgBox("error al conectar con la base de datos , intentelo mas tarde por favor")
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub llenarLabelsTopGeneral()
        Dim cont As Integer = 0
        While dr2.Read()
            arrayPosicion(cont).visible = True
            arrayPuntuacion(cont).visible = True
            arrayNombres(cont).visible = True


            arrayPuntuacion(cont).text = dr2("puntuacion")
            arrayNombres(cont).text = dr2("nombreUsuario").toUpper()



            cont += 1
        End While

    End Sub

    Private Sub lblSalir_Click(sender As Object, e As EventArgs) Handles lblSalir.Click
        My.Computer.Audio.Stop()
        End
    End Sub
End Class