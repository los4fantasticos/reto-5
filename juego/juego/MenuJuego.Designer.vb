﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MenuJuego
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuJuego))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.LblVolver = New System.Windows.Forms.Label()
        Me.LblNuevaPartida = New System.Windows.Forms.Label()
        Me.lblSalir = New System.Windows.Forms.Label()
        Me.panelPosicion = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.labe3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.panelPuntuacion = New System.Windows.Forms.Panel()
        Me.lblPuntuacion1 = New System.Windows.Forms.Label()
        Me.lblPuntuacion2 = New System.Windows.Forms.Label()
        Me.lblPuntuacion3 = New System.Windows.Forms.Label()
        Me.lblPuntuacion4 = New System.Windows.Forms.Label()
        Me.lblPuntuacion5 = New System.Windows.Forms.Label()
        Me.lblPuntuacion6 = New System.Windows.Forms.Label()
        Me.lblPuntuacion7 = New System.Windows.Forms.Label()
        Me.lblPuntuacion8 = New System.Windows.Forms.Label()
        Me.lblPuntuacion9 = New System.Windows.Forms.Label()
        Me.lblPuntuacion10 = New System.Windows.Forms.Label()
        Me.panelNombre = New System.Windows.Forms.Panel()
        Me.lblNombre1 = New System.Windows.Forms.Label()
        Me.lblNombre2 = New System.Windows.Forms.Label()
        Me.lblNombre3 = New System.Windows.Forms.Label()
        Me.lblNombre4 = New System.Windows.Forms.Label()
        Me.lblNombre5 = New System.Windows.Forms.Label()
        Me.lblNombre6 = New System.Windows.Forms.Label()
        Me.lblNombre7 = New System.Windows.Forms.Label()
        Me.lblNombre8 = New System.Windows.Forms.Label()
        Me.lblNombre9 = New System.Windows.Forms.Label()
        Me.lblNombre10 = New System.Windows.Forms.Label()
        Me.panelPosicionJugador = New System.Windows.Forms.Panel()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.panelPuntuacionJugador = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.panelFechasJugador = New System.Windows.Forms.Panel()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.PBoxImagen = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelPosicion.SuspendLayout()
        Me.panelPuntuacion.SuspendLayout()
        Me.panelNombre.SuspendLayout()
        Me.panelPosicionJugador.SuspendLayout()
        Me.panelPuntuacionJugador.SuspendLayout()
        Me.panelFechasJugador.SuspendLayout()
        CType(Me.PBoxImagen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(69, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(164, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Mejores puntuaciones:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(844, 93)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Mis puntuaciones:"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.juego.My.Resources.Resources.LogoReto5
        Me.PictureBox1.Location = New System.Drawing.Point(263, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(575, 111)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'LblVolver
        '
        Me.LblVolver.BackColor = System.Drawing.Color.Transparent
        Me.LblVolver.Image = Global.juego.My.Resources.Resources.volver1
        Me.LblVolver.Location = New System.Drawing.Point(284, 519)
        Me.LblVolver.Name = "LblVolver"
        Me.LblVolver.Size = New System.Drawing.Size(540, 60)
        Me.LblVolver.TabIndex = 10
        '
        'LblNuevaPartida
        '
        Me.LblNuevaPartida.BackColor = System.Drawing.Color.Transparent
        Me.LblNuevaPartida.Image = Global.juego.My.Resources.Resources.nuevapartida1
        Me.LblNuevaPartida.Location = New System.Drawing.Point(284, 432)
        Me.LblNuevaPartida.Name = "LblNuevaPartida"
        Me.LblNuevaPartida.Size = New System.Drawing.Size(540, 60)
        Me.LblNuevaPartida.TabIndex = 11
        '
        'lblSalir
        '
        Me.lblSalir.BackColor = System.Drawing.Color.Transparent
        Me.lblSalir.Image = Global.juego.My.Resources.Resources._exit
        Me.lblSalir.Location = New System.Drawing.Point(30, 25)
        Me.lblSalir.Name = "lblSalir"
        Me.lblSalir.Size = New System.Drawing.Size(30, 30)
        Me.lblSalir.TabIndex = 15
        '
        'panelPosicion
        '
        Me.panelPosicion.BackColor = System.Drawing.Color.Transparent
        Me.panelPosicion.Controls.Add(Me.Label3)
        Me.panelPosicion.Controls.Add(Me.Label4)
        Me.panelPosicion.Controls.Add(Me.labe3)
        Me.panelPosicion.Controls.Add(Me.Label5)
        Me.panelPosicion.Controls.Add(Me.Label6)
        Me.panelPosicion.Controls.Add(Me.Label7)
        Me.panelPosicion.Controls.Add(Me.Label8)
        Me.panelPosicion.Controls.Add(Me.Label9)
        Me.panelPosicion.Controls.Add(Me.Label10)
        Me.panelPosicion.Controls.Add(Me.Label11)
        Me.panelPosicion.Location = New System.Drawing.Point(43, 129)
        Me.panelPosicion.Name = "panelPosicion"
        Me.panelPosicion.Size = New System.Drawing.Size(44, 263)
        Me.panelPosicion.TabIndex = 47
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(15, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(16, 16)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "1"
        Me.Label3.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(15, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(16, 16)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "2"
        Me.Label4.Visible = False
        '
        'labe3
        '
        Me.labe3.AutoSize = True
        Me.labe3.BackColor = System.Drawing.Color.Transparent
        Me.labe3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.labe3.ForeColor = System.Drawing.Color.White
        Me.labe3.Location = New System.Drawing.Point(15, 51)
        Me.labe3.Name = "labe3"
        Me.labe3.Size = New System.Drawing.Size(16, 16)
        Me.labe3.TabIndex = 13
        Me.labe3.Text = "3"
        Me.labe3.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(15, 75)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(16, 16)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "4"
        Me.Label5.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(15, 99)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(16, 16)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "5"
        Me.Label6.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(15, 125)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(16, 16)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "6"
        Me.Label7.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(15, 151)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(16, 16)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "7"
        Me.Label8.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(15, 175)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(16, 16)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "8"
        Me.Label9.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(15, 200)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(16, 16)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "9"
        Me.Label10.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(7, 221)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(24, 16)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "10"
        Me.Label11.Visible = False
        '
        'panelPuntuacion
        '
        Me.panelPuntuacion.BackColor = System.Drawing.Color.Transparent
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion1)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion2)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion3)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion4)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion5)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion6)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion7)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion8)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion9)
        Me.panelPuntuacion.Controls.Add(Me.lblPuntuacion10)
        Me.panelPuntuacion.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panelPuntuacion.ForeColor = System.Drawing.Color.White
        Me.panelPuntuacion.Location = New System.Drawing.Point(111, 129)
        Me.panelPuntuacion.Name = "panelPuntuacion"
        Me.panelPuntuacion.Size = New System.Drawing.Size(89, 263)
        Me.panelPuntuacion.TabIndex = 48
        '
        'lblPuntuacion1
        '
        Me.lblPuntuacion1.AutoSize = True
        Me.lblPuntuacion1.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion1.Location = New System.Drawing.Point(13, 0)
        Me.lblPuntuacion1.Name = "lblPuntuacion1"
        Me.lblPuntuacion1.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion1.TabIndex = 25
        Me.lblPuntuacion1.Text = "Label1"
        Me.lblPuntuacion1.Visible = False
        '
        'lblPuntuacion2
        '
        Me.lblPuntuacion2.AutoSize = True
        Me.lblPuntuacion2.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion2.Location = New System.Drawing.Point(13, 24)
        Me.lblPuntuacion2.Name = "lblPuntuacion2"
        Me.lblPuntuacion2.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion2.TabIndex = 24
        Me.lblPuntuacion2.Text = "Label2"
        Me.lblPuntuacion2.Visible = False
        '
        'lblPuntuacion3
        '
        Me.lblPuntuacion3.AutoSize = True
        Me.lblPuntuacion3.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion3.Location = New System.Drawing.Point(13, 53)
        Me.lblPuntuacion3.Name = "lblPuntuacion3"
        Me.lblPuntuacion3.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion3.TabIndex = 23
        Me.lblPuntuacion3.Text = "Label3"
        Me.lblPuntuacion3.Visible = False
        '
        'lblPuntuacion4
        '
        Me.lblPuntuacion4.AutoSize = True
        Me.lblPuntuacion4.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion4.Location = New System.Drawing.Point(13, 76)
        Me.lblPuntuacion4.Name = "lblPuntuacion4"
        Me.lblPuntuacion4.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion4.TabIndex = 22
        Me.lblPuntuacion4.Text = "Label4"
        Me.lblPuntuacion4.Visible = False
        '
        'lblPuntuacion5
        '
        Me.lblPuntuacion5.AutoSize = True
        Me.lblPuntuacion5.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion5.Location = New System.Drawing.Point(13, 101)
        Me.lblPuntuacion5.Name = "lblPuntuacion5"
        Me.lblPuntuacion5.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion5.TabIndex = 21
        Me.lblPuntuacion5.Text = "Label5"
        Me.lblPuntuacion5.Visible = False
        '
        'lblPuntuacion6
        '
        Me.lblPuntuacion6.AutoSize = True
        Me.lblPuntuacion6.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion6.Location = New System.Drawing.Point(13, 125)
        Me.lblPuntuacion6.Name = "lblPuntuacion6"
        Me.lblPuntuacion6.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion6.TabIndex = 20
        Me.lblPuntuacion6.Text = "Label6"
        Me.lblPuntuacion6.Visible = False
        '
        'lblPuntuacion7
        '
        Me.lblPuntuacion7.AutoSize = True
        Me.lblPuntuacion7.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion7.Location = New System.Drawing.Point(13, 150)
        Me.lblPuntuacion7.Name = "lblPuntuacion7"
        Me.lblPuntuacion7.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion7.TabIndex = 19
        Me.lblPuntuacion7.Text = "Label7"
        Me.lblPuntuacion7.Visible = False
        '
        'lblPuntuacion8
        '
        Me.lblPuntuacion8.AutoSize = True
        Me.lblPuntuacion8.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion8.Location = New System.Drawing.Point(13, 175)
        Me.lblPuntuacion8.Name = "lblPuntuacion8"
        Me.lblPuntuacion8.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion8.TabIndex = 18
        Me.lblPuntuacion8.Text = "Label8"
        Me.lblPuntuacion8.Visible = False
        '
        'lblPuntuacion9
        '
        Me.lblPuntuacion9.AutoSize = True
        Me.lblPuntuacion9.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion9.ForeColor = System.Drawing.Color.White
        Me.lblPuntuacion9.Location = New System.Drawing.Point(13, 200)
        Me.lblPuntuacion9.Name = "lblPuntuacion9"
        Me.lblPuntuacion9.Size = New System.Drawing.Size(50, 16)
        Me.lblPuntuacion9.TabIndex = 17
        Me.lblPuntuacion9.Text = "Label9"
        Me.lblPuntuacion9.Visible = False
        '
        'lblPuntuacion10
        '
        Me.lblPuntuacion10.AutoSize = True
        Me.lblPuntuacion10.BackColor = System.Drawing.Color.Transparent
        Me.lblPuntuacion10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPuntuacion10.Location = New System.Drawing.Point(13, 221)
        Me.lblPuntuacion10.Name = "lblPuntuacion10"
        Me.lblPuntuacion10.Size = New System.Drawing.Size(58, 16)
        Me.lblPuntuacion10.TabIndex = 16
        Me.lblPuntuacion10.Text = "Label10"
        Me.lblPuntuacion10.Visible = False
        '
        'panelNombre
        '
        Me.panelNombre.BackColor = System.Drawing.Color.Transparent
        Me.panelNombre.Controls.Add(Me.lblNombre1)
        Me.panelNombre.Controls.Add(Me.lblNombre2)
        Me.panelNombre.Controls.Add(Me.lblNombre3)
        Me.panelNombre.Controls.Add(Me.lblNombre4)
        Me.panelNombre.Controls.Add(Me.lblNombre5)
        Me.panelNombre.Controls.Add(Me.lblNombre6)
        Me.panelNombre.Controls.Add(Me.lblNombre7)
        Me.panelNombre.Controls.Add(Me.lblNombre8)
        Me.panelNombre.Controls.Add(Me.lblNombre9)
        Me.panelNombre.Controls.Add(Me.lblNombre10)
        Me.panelNombre.Location = New System.Drawing.Point(206, 129)
        Me.panelNombre.Name = "panelNombre"
        Me.panelNombre.Size = New System.Drawing.Size(104, 263)
        Me.panelNombre.TabIndex = 49
        '
        'lblNombre1
        '
        Me.lblNombre1.AutoSize = True
        Me.lblNombre1.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre1.ForeColor = System.Drawing.Color.White
        Me.lblNombre1.Location = New System.Drawing.Point(3, 0)
        Me.lblNombre1.Name = "lblNombre1"
        Me.lblNombre1.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre1.TabIndex = 35
        Me.lblNombre1.Text = "Nombre1"
        Me.lblNombre1.Visible = False
        '
        'lblNombre2
        '
        Me.lblNombre2.AutoSize = True
        Me.lblNombre2.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre2.ForeColor = System.Drawing.Color.White
        Me.lblNombre2.Location = New System.Drawing.Point(3, 24)
        Me.lblNombre2.Name = "lblNombre2"
        Me.lblNombre2.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre2.TabIndex = 34
        Me.lblNombre2.Text = "Nombre2"
        Me.lblNombre2.Visible = False
        '
        'lblNombre3
        '
        Me.lblNombre3.AutoSize = True
        Me.lblNombre3.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre3.ForeColor = System.Drawing.Color.White
        Me.lblNombre3.Location = New System.Drawing.Point(3, 53)
        Me.lblNombre3.Name = "lblNombre3"
        Me.lblNombre3.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre3.TabIndex = 33
        Me.lblNombre3.Text = "Nombre3"
        Me.lblNombre3.Visible = False
        '
        'lblNombre4
        '
        Me.lblNombre4.AutoSize = True
        Me.lblNombre4.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre4.ForeColor = System.Drawing.Color.White
        Me.lblNombre4.Location = New System.Drawing.Point(3, 76)
        Me.lblNombre4.Name = "lblNombre4"
        Me.lblNombre4.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre4.TabIndex = 32
        Me.lblNombre4.Text = "Nombre4"
        Me.lblNombre4.Visible = False
        '
        'lblNombre5
        '
        Me.lblNombre5.AutoSize = True
        Me.lblNombre5.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre5.ForeColor = System.Drawing.Color.White
        Me.lblNombre5.Location = New System.Drawing.Point(3, 101)
        Me.lblNombre5.Name = "lblNombre5"
        Me.lblNombre5.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre5.TabIndex = 31
        Me.lblNombre5.Text = "Nombre5"
        Me.lblNombre5.Visible = False
        '
        'lblNombre6
        '
        Me.lblNombre6.AutoSize = True
        Me.lblNombre6.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre6.ForeColor = System.Drawing.Color.White
        Me.lblNombre6.Location = New System.Drawing.Point(3, 127)
        Me.lblNombre6.Name = "lblNombre6"
        Me.lblNombre6.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre6.TabIndex = 30
        Me.lblNombre6.Text = "Nombre6"
        Me.lblNombre6.Visible = False
        '
        'lblNombre7
        '
        Me.lblNombre7.AutoSize = True
        Me.lblNombre7.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre7.ForeColor = System.Drawing.Color.White
        Me.lblNombre7.Location = New System.Drawing.Point(3, 150)
        Me.lblNombre7.Name = "lblNombre7"
        Me.lblNombre7.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre7.TabIndex = 29
        Me.lblNombre7.Text = "Nombre7"
        Me.lblNombre7.Visible = False
        '
        'lblNombre8
        '
        Me.lblNombre8.AutoSize = True
        Me.lblNombre8.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre8.ForeColor = System.Drawing.Color.White
        Me.lblNombre8.Location = New System.Drawing.Point(3, 175)
        Me.lblNombre8.Name = "lblNombre8"
        Me.lblNombre8.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre8.TabIndex = 28
        Me.lblNombre8.Text = "Nombre8"
        Me.lblNombre8.Visible = False
        '
        'lblNombre9
        '
        Me.lblNombre9.AutoSize = True
        Me.lblNombre9.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre9.ForeColor = System.Drawing.Color.White
        Me.lblNombre9.Location = New System.Drawing.Point(3, 200)
        Me.lblNombre9.Name = "lblNombre9"
        Me.lblNombre9.Size = New System.Drawing.Size(65, 16)
        Me.lblNombre9.TabIndex = 27
        Me.lblNombre9.Text = "Nombre9"
        Me.lblNombre9.Visible = False
        '
        'lblNombre10
        '
        Me.lblNombre10.AutoSize = True
        Me.lblNombre10.BackColor = System.Drawing.Color.Transparent
        Me.lblNombre10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre10.ForeColor = System.Drawing.Color.White
        Me.lblNombre10.Location = New System.Drawing.Point(3, 221)
        Me.lblNombre10.Name = "lblNombre10"
        Me.lblNombre10.Size = New System.Drawing.Size(73, 16)
        Me.lblNombre10.TabIndex = 26
        Me.lblNombre10.Text = "Nombre10"
        Me.lblNombre10.Visible = False
        '
        'panelPosicionJugador
        '
        Me.panelPosicionJugador.BackColor = System.Drawing.Color.Transparent
        Me.panelPosicionJugador.Controls.Add(Me.Label12)
        Me.panelPosicionJugador.Controls.Add(Me.Label13)
        Me.panelPosicionJugador.Controls.Add(Me.Label14)
        Me.panelPosicionJugador.Controls.Add(Me.Label15)
        Me.panelPosicionJugador.Controls.Add(Me.Label16)
        Me.panelPosicionJugador.Controls.Add(Me.Label17)
        Me.panelPosicionJugador.Controls.Add(Me.Label18)
        Me.panelPosicionJugador.Controls.Add(Me.Label19)
        Me.panelPosicionJugador.Controls.Add(Me.Label20)
        Me.panelPosicionJugador.Controls.Add(Me.Label21)
        Me.panelPosicionJugador.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panelPosicionJugador.Location = New System.Drawing.Point(776, 129)
        Me.panelPosicionJugador.Name = "panelPosicionJugador"
        Me.panelPosicionJugador.Size = New System.Drawing.Size(36, 263)
        Me.panelPosicionJugador.TabIndex = 50
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(15, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(16, 16)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "1"
        Me.Label12.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.BackColor = System.Drawing.Color.Transparent
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(15, 25)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(16, 16)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "2"
        Me.Label13.Visible = False
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.White
        Me.Label14.Location = New System.Drawing.Point(15, 51)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(16, 16)
        Me.Label14.TabIndex = 13
        Me.Label14.Text = "3"
        Me.Label14.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.White
        Me.Label15.Location = New System.Drawing.Point(16, 75)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(16, 16)
        Me.Label15.TabIndex = 12
        Me.Label15.Text = "4"
        Me.Label15.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(16, 100)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(16, 16)
        Me.Label16.TabIndex = 11
        Me.Label16.Text = "5"
        Me.Label16.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.White
        Me.Label17.Location = New System.Drawing.Point(16, 125)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(16, 16)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "6"
        Me.Label17.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.BackColor = System.Drawing.Color.Transparent
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(16, 150)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(16, 16)
        Me.Label18.TabIndex = 9
        Me.Label18.Text = "7"
        Me.Label18.Visible = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(16, 175)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(16, 16)
        Me.Label19.TabIndex = 8
        Me.Label19.Text = "8"
        Me.Label19.Visible = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.White
        Me.Label20.Location = New System.Drawing.Point(16, 200)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(16, 16)
        Me.Label20.TabIndex = 7
        Me.Label20.Text = "9"
        Me.Label20.Visible = False
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(8, 221)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(24, 16)
        Me.Label21.TabIndex = 6
        Me.Label21.Text = "10"
        Me.Label21.Visible = False
        '
        'panelPuntuacionJugador
        '
        Me.panelPuntuacionJugador.BackColor = System.Drawing.Color.Transparent
        Me.panelPuntuacionJugador.Controls.Add(Me.Label22)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label23)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label24)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label25)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label26)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label27)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label28)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label29)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label30)
        Me.panelPuntuacionJugador.Controls.Add(Me.Label31)
        Me.panelPuntuacionJugador.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panelPuntuacionJugador.ForeColor = System.Drawing.Color.White
        Me.panelPuntuacionJugador.Location = New System.Drawing.Point(818, 129)
        Me.panelPuntuacionJugador.Name = "panelPuntuacionJugador"
        Me.panelPuntuacionJugador.Size = New System.Drawing.Size(74, 263)
        Me.panelPuntuacionJugador.TabIndex = 51
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(13, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(50, 16)
        Me.Label22.TabIndex = 25
        Me.Label22.Text = "Label1"
        Me.Label22.Visible = False
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(13, 25)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(50, 16)
        Me.Label23.TabIndex = 24
        Me.Label23.Text = "Label2"
        Me.Label23.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(13, 51)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(50, 16)
        Me.Label24.TabIndex = 23
        Me.Label24.Text = "Label3"
        Me.Label24.Visible = False
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(13, 75)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(50, 16)
        Me.Label25.TabIndex = 22
        Me.Label25.Text = "Label4"
        Me.Label25.Visible = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(13, 100)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(50, 16)
        Me.Label26.TabIndex = 21
        Me.Label26.Text = "Label5"
        Me.Label26.Visible = False
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(13, 125)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(50, 16)
        Me.Label27.TabIndex = 20
        Me.Label27.Text = "Label6"
        Me.Label27.Visible = False
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(13, 150)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(50, 16)
        Me.Label28.TabIndex = 19
        Me.Label28.Text = "Label7"
        Me.Label28.Visible = False
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.BackColor = System.Drawing.Color.Transparent
        Me.Label29.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(13, 175)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(50, 16)
        Me.Label29.TabIndex = 18
        Me.Label29.Text = "Label8"
        Me.Label29.Visible = False
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.BackColor = System.Drawing.Color.Transparent
        Me.Label30.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.White
        Me.Label30.Location = New System.Drawing.Point(13, 200)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(50, 16)
        Me.Label30.TabIndex = 17
        Me.Label30.Text = "Label9"
        Me.Label30.Visible = False
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.BackColor = System.Drawing.Color.Transparent
        Me.Label31.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(13, 221)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(58, 16)
        Me.Label31.TabIndex = 16
        Me.Label31.Text = "Label10"
        Me.Label31.Visible = False
        '
        'panelFechasJugador
        '
        Me.panelFechasJugador.BackColor = System.Drawing.Color.Transparent
        Me.panelFechasJugador.Controls.Add(Me.Label32)
        Me.panelFechasJugador.Controls.Add(Me.Label33)
        Me.panelFechasJugador.Controls.Add(Me.Label34)
        Me.panelFechasJugador.Controls.Add(Me.Label35)
        Me.panelFechasJugador.Controls.Add(Me.Label36)
        Me.panelFechasJugador.Controls.Add(Me.Label37)
        Me.panelFechasJugador.Controls.Add(Me.Label38)
        Me.panelFechasJugador.Controls.Add(Me.Label39)
        Me.panelFechasJugador.Controls.Add(Me.Label40)
        Me.panelFechasJugador.Controls.Add(Me.Label41)
        Me.panelFechasJugador.Location = New System.Drawing.Point(895, 129)
        Me.panelFechasJugador.Name = "panelFechasJugador"
        Me.panelFechasJugador.Size = New System.Drawing.Size(145, 263)
        Me.panelFechasJugador.TabIndex = 52
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.BackColor = System.Drawing.Color.Transparent
        Me.Label32.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.White
        Me.Label32.Location = New System.Drawing.Point(3, 1)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(62, 14)
        Me.Label32.TabIndex = 35
        Me.Label32.Text = "Nombre1"
        Me.Label32.Visible = False
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.BackColor = System.Drawing.Color.Transparent
        Me.Label33.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.White
        Me.Label33.Location = New System.Drawing.Point(3, 26)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(62, 14)
        Me.Label33.TabIndex = 34
        Me.Label33.Text = "Nombre2"
        Me.Label33.Visible = False
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.Color.Transparent
        Me.Label34.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.White
        Me.Label34.Location = New System.Drawing.Point(3, 53)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(62, 14)
        Me.Label34.TabIndex = 33
        Me.Label34.Text = "Nombre3"
        Me.Label34.Visible = False
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.Color.Transparent
        Me.Label35.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.White
        Me.Label35.Location = New System.Drawing.Point(3, 77)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(62, 14)
        Me.Label35.TabIndex = 32
        Me.Label35.Text = "Nombre4"
        Me.Label35.Visible = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.Color.Transparent
        Me.Label36.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.White
        Me.Label36.Location = New System.Drawing.Point(3, 101)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(62, 14)
        Me.Label36.TabIndex = 31
        Me.Label36.Text = "Nombre5"
        Me.Label36.Visible = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.BackColor = System.Drawing.Color.Transparent
        Me.Label37.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.White
        Me.Label37.Location = New System.Drawing.Point(3, 127)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(62, 14)
        Me.Label37.TabIndex = 30
        Me.Label37.Text = "Nombre6"
        Me.Label37.Visible = False
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.BackColor = System.Drawing.Color.Transparent
        Me.Label38.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.Color.White
        Me.Label38.Location = New System.Drawing.Point(3, 152)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(62, 14)
        Me.Label38.TabIndex = 29
        Me.Label38.Text = "Nombre7"
        Me.Label38.Visible = False
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.BackColor = System.Drawing.Color.Transparent
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.Color.White
        Me.Label39.Location = New System.Drawing.Point(3, 177)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(62, 14)
        Me.Label39.TabIndex = 28
        Me.Label39.Text = "Nombre8"
        Me.Label39.Visible = False
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.BackColor = System.Drawing.Color.Transparent
        Me.Label40.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.White
        Me.Label40.Location = New System.Drawing.Point(3, 202)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(62, 14)
        Me.Label40.TabIndex = 27
        Me.Label40.Text = "Nombre9"
        Me.Label40.Visible = False
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.BackColor = System.Drawing.Color.Transparent
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.Color.White
        Me.Label41.Location = New System.Drawing.Point(3, 222)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(70, 14)
        Me.Label41.TabIndex = 26
        Me.Label41.Text = "Nombre10"
        Me.Label41.Visible = False
        '
        'PBoxImagen
        '
        Me.PBoxImagen.Location = New System.Drawing.Point(350, 169)
        Me.PBoxImagen.Name = "PBoxImagen"
        Me.PBoxImagen.Size = New System.Drawing.Size(394, 170)
        Me.PBoxImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBoxImagen.TabIndex = 0
        Me.PBoxImagen.TabStop = False
        '
        'MenuJuego
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1099, 620)
        Me.Controls.Add(Me.panelFechasJugador)
        Me.Controls.Add(Me.panelPuntuacionJugador)
        Me.Controls.Add(Me.panelPosicionJugador)
        Me.Controls.Add(Me.panelNombre)
        Me.Controls.Add(Me.panelPuntuacion)
        Me.Controls.Add(Me.panelPosicion)
        Me.Controls.Add(Me.lblSalir)
        Me.Controls.Add(Me.LblNuevaPartida)
        Me.Controls.Add(Me.LblVolver)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PBoxImagen)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MenuJuego"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelPosicion.ResumeLayout(False)
        Me.panelPosicion.PerformLayout()
        Me.panelPuntuacion.ResumeLayout(False)
        Me.panelPuntuacion.PerformLayout()
        Me.panelNombre.ResumeLayout(False)
        Me.panelNombre.PerformLayout()
        Me.panelPosicionJugador.ResumeLayout(False)
        Me.panelPosicionJugador.PerformLayout()
        Me.panelPuntuacionJugador.ResumeLayout(False)
        Me.panelPuntuacionJugador.PerformLayout()
        Me.panelFechasJugador.ResumeLayout(False)
        Me.panelFechasJugador.PerformLayout()
        CType(Me.PBoxImagen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents LblVolver As Label
    Friend WithEvents LblNuevaPartida As Label
    Friend WithEvents lblSalir As Label
    Friend WithEvents panelPosicion As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents labe3 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents panelPuntuacion As Panel
    Friend WithEvents lblPuntuacion1 As Label
    Friend WithEvents lblPuntuacion2 As Label
    Friend WithEvents lblPuntuacion3 As Label
    Friend WithEvents lblPuntuacion4 As Label
    Friend WithEvents lblPuntuacion5 As Label
    Friend WithEvents lblPuntuacion6 As Label
    Friend WithEvents lblPuntuacion7 As Label
    Friend WithEvents lblPuntuacion8 As Label
    Friend WithEvents lblPuntuacion9 As Label
    Friend WithEvents lblPuntuacion10 As Label
    Friend WithEvents panelNombre As Panel
    Friend WithEvents lblNombre1 As Label
    Friend WithEvents lblNombre2 As Label
    Friend WithEvents lblNombre3 As Label
    Friend WithEvents lblNombre4 As Label
    Friend WithEvents lblNombre5 As Label
    Friend WithEvents lblNombre6 As Label
    Friend WithEvents lblNombre7 As Label
    Friend WithEvents lblNombre8 As Label
    Friend WithEvents lblNombre9 As Label
    Friend WithEvents lblNombre10 As Label
    Friend WithEvents panelPosicionJugador As Panel
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents panelPuntuacionJugador As Panel
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents panelFechasJugador As Panel
    Friend WithEvents Label32 As Label
    Friend WithEvents Label33 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label36 As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents Label40 As Label
    Friend WithEvents Label41 As Label
    Friend WithEvents PBoxImagen As PictureBox
End Class
