﻿Imports Oracle.ManagedDataAccess.Client
Public Class pantallaFinal
    Dim arrayPosicion As ArrayList
    Dim arrayPuntuacion As ArrayList
    Dim arrayNombres As ArrayList
    Dim arrayTipoJuego As ArrayList

    Dim dr As OracleDataReader
    Dim t As Integer


    Private Sub pantallaFinal_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        My.Computer.Audio.Play(My.Resources.fin, AudioPlayMode.BackgroundLoop)
        Me.Width = pregunta.Width
        Me.Height = pregunta.Height
        Me.Location = pregunta.Location
        lblPuntuacion.Text = pregunta.puntuacion

        arrayPosicion = New ArrayList
        arrayPuntuacion = New ArrayList
        arrayNombres = New ArrayList
        arrayTipoJuego = New ArrayList

        For Each p As Label In panelPosicion.Controls
            arrayPosicion.Add(p)
        Next
        For Each p As Label In panelPuntuacion.Controls
            arrayPuntuacion.Add(p)

        Next
        For Each p As Label In panelNombre.Controls
            arrayNombres.Add(p)

        Next
        For Each p As Label In panelTipo.Controls
            arrayTipoJuego.Add(p)

        Next


        Dim command As OracleCommand
        command = New OracleCommand("select sum(score) as puntuacion,fecha,usuarios.nombre as nombreUsuario,tipo_juego.nombre as tipoJuego 
from pregunpar inner join partida on partida.id_partida=pregunpar.id_partida 
inner join usuarios on usuarios.id_usuario=partida.id_usuario 
inner join tipo_juego on partida.id_tipo_juego=tipo_juego.id_tipo_juego 
 group by partida.id_partida , fecha,usuarios.nombre,tipo_juego.nombre order by puntuacion desc FETCH FIRST 10 ROWS ONLY", Form1.oracleConnection1)
        Try
            dr = command.ExecuteReader()

            If dr.HasRows Then
                llenarLabels()
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        comprobarRecord()

        t = 0

    End Sub

    Private Sub comprobarRecord()
        For Each p In arrayPuntuacion
            If Val(pregunta.puntuacion) >= Val(p.text) Then
                lblRecord.Visible = True
                PBMonkey.Visible = True
                Timer1.Start()
            End If
        Next


        'RESALTAR RECORD
        Dim cont As Integer = 0
        For Each p In arrayPuntuacion

            If Val(pregunta.puntuacion) = Val(p.text) And Form1.usuario.ToLower = arrayNombres(cont).text.tolower And TipoJuego.tipo.ToLower = arrayTipoJuego(cont).text.tolower Then


                p.BackColor = Color.Red
                arrayPosicion(cont).BackColor = Color.Red
                arrayNombres(cont).BackColor = Color.Red
                arrayTipoJuego(cont).BackColor = Color.Red

                Exit For
            End If
            cont += 1
        Next
    End Sub

    Private Sub llenarLabels()
        Dim cont As Integer = 0
        While dr.Read()
            arrayPosicion(cont).visible = True
            arrayPuntuacion(cont).visible = True
            arrayNombres(cont).visible = True
            arrayTipoJuego(cont).visible = True

            arrayPuntuacion(cont).text = dr("puntuacion")
            arrayNombres(cont).text = dr("nombreUsuario").toUpper()
            arrayTipoJuego(cont).text = dr("tipoJuego").toUpper()


            cont += 1
        End While


    End Sub

    Private Sub LblVolverJugar_Click(sender As Object, e As EventArgs) Handles LblVolverJugar.Click
        Me.Close()
        pregunta.Close()
        MenuJuego.Close()
        TipoJuego.Show()
        My.Computer.Audio.Stop()
        My.Computer.Audio.Play(My.Resources.intro, AudioPlayMode.BackgroundLoop)
    End Sub

    Private Sub LblSalir_Click(sender As Object, e As EventArgs) Handles LblSalir.Click
        Me.Hide()
        Form2.Show()

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        t = t + 1
        If t = 1 Then
            lblRecord.ForeColor = Color.Green

        Else
            lblRecord.ForeColor = Color.White
            t = 0
        End If
    End Sub


    Private Sub LblSalir_MouseLeave(sender As Object, e As EventArgs) Handles LblSalir.MouseLeave
        LblSalir.Image = My.Resources.Exit1OK
    End Sub


    Private Sub LblVolverJugar_MouseLeave(sender As Object, e As EventArgs) Handles LblVolverJugar.MouseLeave
        LblVolverJugar.Image = My.Resources.Replay1OK
    End Sub

    Private Sub LblSalir_MouseEnter(sender As Object, e As EventArgs) Handles LblSalir.MouseEnter
        LblSalir.Image = My.Resources.Exit2OK
    End Sub

    Private Sub LblVolverJugar_MouseEnter(sender As Object, e As EventArgs) Handles LblVolverJugar.MouseEnter
        LblVolverJugar.Image = My.Resources.Replay2OK
    End Sub
End Class